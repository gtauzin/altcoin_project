import pandas as pd
import numpy as np
import time
import datetime

import utils
import random
import os
import sys

sys.path.insert(1, './machine_learning/')
import dataSampler as dS

sys.path.insert(1, './markets/')
from poloniex3 import Poloniex3


class Market(object):
# TODO: Implement strategy options as recipeDictionnary

    def __init__(self, currencyPair, tradingPeriod,
                 featureCurrencyPairList, samplingFrequency,
                 nPeriodRequired,
                 buyPriceStrategy='randomIncrease',
                 sellPriceStrategy='randomDecrease',
                 buyAmountStrategy='smallDecrement',
                 sellAmountStrategy='sellAll',
                 orderType='immediateOrCancel'):
        self.currencyPair = currencyPair
        self.tradingPeriod = tradingPeriod
        self.featureCurrencyPairList = featureCurrencyPairList
        self.samplingFrequency = samplingFrequency
        self.nPeriodRequired = nPeriodRequired

        self.buyPriceStrategy = buyPriceStrategy
        self.sellPriceStrategy = sellPriceStrategy
        self.buyAmountStrategy = buyAmountStrategy
        self.sellAmountStrategy = sellAmountStrategy
        self.orderType = orderType

    def compute_buy_price(self, marketAskPrice):
        # TODO: Determine the range of these quantities exactly
        if (self.buyPriceStrategy == "randomIncrease"):
            increment = random.uniform(0.001,0.01)

        buyPrice = marketAskPrice * (1 + increment)
        return buyPrice

    def compute_sell_price(self, marketBidPrice):
        # TODO: Determine the range of these quantities exactly
        if (self.sellPriceStrategy == "randomDecrease"):
            decrement = random.uniform(0.001,0.01)

        sellPrice = marketBidPrice * (1 - decrement)
        return sellPrice

    def compute_buy_amount(self, balance, currentRate):
        # TODO determine the range of these quantities exactly
        if (self.buyAmountStrategy == "smallDecrement"):
            decrement = 0.005

        buyAmount = balance * (1.0/currentRate) * (1 - decrement)
        return buyAmount

    def compute_sell_amount(self, boughtAmount):
        if (self.sellAmountStrategy == "sellAll"):
            sellAmount = boughtAmount

        return sellAmount

#TODO: Implement implicit period incrementation
class VirtualMarket(Market):
    def __init__(self, currencyPair, tradingPeriod,
                 featureCurrencyPairList, samplingFrequency,
                 nPeriodRequired, maxFee, X):
        super(VirtualMarket, self).__init__(currencyPair, tradingPeriod,
                                            featureCurrencyPairList, samplingFrequency,
                                            nPeriodRequired)
        self.maxFee = maxFee
        self.primaryCurrencyHolding = 1.0
        self.secondaryCurrencyHolding = 0.0

        self.X = X

        self.closePrice = dS.DataSampler([self.currencyPair], self.samplingFrequency,
                                         ['close']).transform(X)

        self.stepTime = pd.Timedelta(self.samplingFrequency)
        self.startTime = pd.Timestamp(self.X.index[0])# + self.nPeriodRequired * self.stepTime
        self.endTime = self.startTime + (self.nPeriodRequired+ 0.40) * self.stepTime
        self.finalTime = pd.Timestamp(self.X.index[-1])

    def is_final_iteration(self):
        return self.endTime >= self.finalTime

    def buy(self):
        #TODO: Check that this is indeed the lowest asked price
        marketAskPrice = self.querry_current_price(self.endTime)
        buyPrice = self.compute_sell_price(marketAskPrice)

        buyAmount = self.compute_sell_amount(self.primaryCurrencyHolding)

        fee = self.maxFee
        #TODO: Check that this is divided by price
        self.boughtAmount = buyAmount * buyPrice * (1 - fee)

        self.primaryCurrencyHolding -= buyAmount
        self.secondaryCurrencyHolding += self.boughtAmount

        return self.boughtAmount

    def sell(self):
        #TODO: Check that this is indeed the highest bid price
        marketBidPrice = self.querry_current_price(self.endTime+pd.Timedelta(self.tradingPeriod))
        sellPrice = self.compute_sell_price(marketBidPrice)

        sellAmount = self.compute_sell_amount(self.secondaryCurrencyHolding)

        fee = self.maxFee
        #TODO: Check that this is divided by price
        soldAmount = sellAmount/sellPrice * (1 - fee)

        self.secondaryCurrencyHolding -= sellAmount
        self.primaryCurrencyHolding += soldAmount

        return soldAmount

    def querry_current_price(self, currentTime):
        querryTime = self.closePrice.index.asof(currentTime)
        currentPrice = float(self.closePrice.loc[querryTime])
        print("Current time: ", currentTime, ", price: ", currentPrice)
        return currentPrice

    def query_current_raw_data(self):
        currentX = self.X[str(self.startTime):str(self.endTime)]
        self.startTime += self.stepTime
        self.endTime += self.stepTime
        return currentX


class VirtualPoloniex(VirtualMarket):
    def __init__(self, currencyPair, tradingPeriod, featureCurrencyPairList,
                 samplingFrequency, nPeriodRequired, X):
        super(VirtualPoloniex, self).__init__(currencyPair, tradingPeriod,
                                              featureCurrencyPairList,
                                              samplingFrequency=samplingFrequency,
                                              nPeriodRequired=nPeriodRequired,
                                              maxFee=0.0025, X=X)


class RealMarket(Market):
    def __init__(self, currencyPair, tradingPeriod, featureCurrencyPairList,
                 samplingFrequency, nPeriodRequired):
        super(RealMarket, self).__init__(currencyPair=currencyPair,
                                         tradingPeriod=tradingPeriod,
                                         featureCurrencyPairList=featureCurrencyPairList,
                                         samplingFrequency=samplingFrequency,
                                         nPeriodRequired=nPeriodRequired)

    @staticmethod
    def get_key(marketName):
        homeDirectory = os.environ['HOME']
        with open(homeDirectory + '/phd_thesis_k/' + marketName, 'r') as f:
            return f.readline()[:-1]  #remove the final invisible \n

    @staticmethod
    def get_secret(marketName):
        homeDirectory = os.environ['HOME']
        with open(homeDirectory + '/phd_thesis_s/' + marketName, 'r') as f:
            return f.readline()[:-1]  #remove the final invisible \n


class RealPoloniex(RealMarket, Poloniex3):
    def __init__(self, currencyPair, tradingPeriod, featureCurrencyPairList,
                 samplingFrequency, nPeriodRequired):
        key = RealMarket.get_key('poloniex')
        secret = RealMarket.get_secret('poloniex')
        super(RealPoloniex, self).__init__(currencyPair=currencyPair,
                                           tradingPeriod=tradingPeriod,
                                           featureCurrencyPairList=featureCurrencyPairList,
                                           samplingFrequency=samplingFrequency,
                                           nPeriodRequired=nPeriodRequired,
                                           key=key, secret=secret)
        self.primaryCurrencyHolding = Poloniex3.returnBalances(self)['BTC']
        secondaryCurrency = currencyPair.split('_')[1]
        self.secondaryCurrencyHolding = Poloniex3.returnBalances(self)[secondaryCurrency]

        self.endTime = pd.to_datetime('now')
        self.stepTime = pd.Timedelta(samplingFrequency)
        self.requiredPeriod = nPeriodRequired * self.stepTime
        self.startTime = self.endTime - self.requiredPeriod

        self.XList = self.init_raw_DataFrame()

    def init_raw_DataFrame(self):
        #sleep(self.timeStep.total_seconds())
        queryTimeDuration = '1h'

        initXList = []
        tradeHistoryList = []

        #TODO: Querry just the right amount of data

        for currencyPair in self.featureCurrencyPairList:
            for time in pd.date_range(self.startTime, self.endTime,
                                        freq=querryTimeDuration):
                unixStartTime = utils.utcTime_2_unixTime(time)
                unixEndTime = utils.utcTime_2_unixTime(time+pd.Timedelta(querryTimeDuration))

                tradeHistoryListTemp = Poloniex3.marketTradeHist(self, currencyPair,
                                                                 start=unixStartTime,
                                                                 end=unixEndTime)
                tradeHistoryList.append(tradeHistoryListTemp)
            initXList.append(pd.DataFrame(tradeHistoryDict).T)

            return initXList


    def buy(self):
        #TODO: Check that this is indeed the lowest asked price
        marketAskPrice = Poloniex3.returnOrderBook(self, self.currencyPair)['asks'][0][0]
        buyPrice = self.compute_sell_price(marketAskPrice)

        buyAmount = self.compute_sell_amount(self.primaryCurrencyHolding)

        try:
            buy = Poloniex3.buy(self, self.currencyPair, price, amount, orderType=orderType)
            self.boughtTime = time.time()
            time.sleep(5)
        except KeyError as e:
            raise Exception("Something went wrong with the buy order: %s" % buy['error'])

        fee = Poloniex3.returnFeeInfo(self)['takerFee']
        self.boughtAmount = buyAmount * buyPrice * (1 - fee)

        self.primaryCurrencyHolding -= buyAmount
        self.secondaryCurrencyHolding += boughtAmount

        return self.boughtAmount

    def sell(self):
        sleepTime = pd.Timedelta(self.TradingPeriod).total_seconds() \
                    - (self.boughtTime - time.time())
        sleep(sleepTime)

        #TODO: Check that this is indeed the highest bid price
        marketBidPrice = Poloniex3.returnOrderBook(self, self.currencyPair)['bids'][0][0]
        sellPrice = self.compute_sell_price(marketBidPrice)

        sellAmount = self.compute_sell_amount(self.secondaryCurrencyHolding)

        try:
            sell = Poloniex3.sell(self, self.currencyPair, sellPrice, sellAmount,
                                  self.orderType)
            self.soldTime = time.time()
        except KeyError as e:
            raise Exception("Something went wrong with the buy order: %s" % sell['error'])

        fee = Poloniex3.returnFeeInfo(self)['takerFee']
        soldAmount = sellAmount/sellPrice * (1 - fee)

        #TODO: Add check with real balance
        self.secondaryCurrencyHolding -= sellAmount
        self.primaryCurrencyHolding += soldAmount

        return soldAmount

    def query_current_raw_data(self):
        sleep(self.timeStep.total_seconds())

        self.startTime = self.endTime
        self.endTime = pd.to_datetime('now')


        initXList = []

        #TODO: Querry just the right amount of data
        unixStartTime = utils.utcTime_2_unixTime(utcTime)
        unixEndTime = utils.utcTime_2_unixTime(utcTime)

        for currencyPair in self.featureCurrencyPairList:
            tradeHistoryDict = \
                Poloniex3.marketTradeHist(self, currencyPair,
                                          start=unixStartTime,
                                          end=unixEndTime)
            rawDataFrame = pd.DataFrame(tradeHistoryDict).T[self.startTime:self.endTime]
            initXList.append(rawDataFrame)
        return initXList

class DiagnosticPoloniex(RealPoloniex):
    def __init__(self, period):
        super(DiagnosticPoloniex, self).__init__(period)
