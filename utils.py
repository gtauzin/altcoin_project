import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pandas.util.testing import assert_frame_equal
import time, json
import urllib.request, urllib.error
import sys
sys.path.insert(1, './markets/')
from collections import defaultdict
#import poloniex3
#from poloniex3 import Poloniex3
import datetime
from datetime import datetime
from datetime import timedelta
from dateutil import tz
# Declaring useful directory paths
current_dir_path = os.path.dirname(os.path.realpath(__file__))
input_data_dir_path = os.path.join(current_dir_path, 'input/')
# init polonix object
#polo = poloniex3.Poloniex3()

def calculate_current_features(requiredRawDataframe, finalFeatureList, currencyPairs, returnPeriodList):
	'''
	To be used both to generate the train-test dataframe and as a single realtime data point to predict a new  instance.
	This function determines the order in which the features appear in the test-train dataframe.
	:param requiredRawDataframe: Reduced Raw Dataframe (close and volumes) including only as many rows are needed to compute
	the features to predict. Using n- day returns, the number of rows will be approximately max(returnPeriodList).
	:param finalFeatureList: list of all the feature effectively used. After grid search and feature selection this list will be in
	general shorter than the list used to build the complete dataframe for train-test.
	:param currencyPairs: All the currencyPairs appearing in the dataframe, including the ones to forecast
	:param returnPeriodList: list with the all the n chosen for the nDay-return features
	:return: list with values of the features, it's needed  to predict a new instance.
	'''

	current_features = []

	for currencyPair in currencyPairs:
		# compute volumes
		if "Volume " + currencyPair in finalFeatureList:
			volume = requiredRawDataframe["Volume " + currencyPair].iloc[-1]  # most recent volume
			current_features.append(volume)

		# compute n-day returns
		for n in returnPeriodList:
			if str(n) + "D " + currencyPair in finalFeatureList:
				last_close = float(requiredRawDataframe["Close " + currencyPair].iloc[-1])
				opening_price = float(requiredRawDataframe["Close " + currencyPair].iloc[-(n + 1)])
				n_day_return = gain(last_close, opening_price)

				current_features.append(n_day_return)

	return current_features


def setup_dataframe_refactored(rawDataframeClose, rawDataframeVolume,
							   currencyPairToForecast, currencyPairsToTrain, dates, period,
							   percentageTresholdsList=[0.7],
							   returnPeriodList=[1, 2, 3, 7, 14, 48, 144, 336],
							   postfix=""):
	'''
	:param rawDataframeClose: refers to all the dates
	:param rawDataframeVolume: refers to all the dates
	:param currencyPairToForecast:
	:param currencyPairsToTrain:
	:param dates:
	:param period:
	:param percentageTresholdsList:
	:param returnPeriodList:
	:param postfix:
	:return:
	'''

	# For testing
	# startDate = '2017-07-15-22:00:00'
	# endDate = '2017-07-15-23:00:00'
	# dates = pd.date_range(startDate, endDate, freq=period)

	# close + volumes
	fullRawDataframe = rawDataframeClose.join(rawDataframeVolume, how='inner')

	# all the possible features
	featureList = generate_feature_labels(currencyPairsToTrain, returnPeriodList)

	# define an empty dataframe with no columns and index given by dates, in the format 2017-10-07 14:54:43
	columns = featureList

	dataFrame = pd.DataFrame(index=dates, columns=columns)

	requiredRawDataframe = fullRawDataframe

	# fill the dataframe
	for row_number in range(len(dataFrame.index)):
		new_row = calculate_current_features(requiredRawDataframe, featureList, currencyPairsToTrain, returnPeriodList)
		# print("NEWROW ", new_row)

		# add a new row to the dataframe from below
		dataFrame.loc[dates[-(row_number + 1)]] = pd.Series(dict(zip(columns, new_row)))

		# remove the last row (the most recent) of the dataframe, so that we loop into past data
		requiredRawDataframe = requiredRawDataframe[:-1]

	# print(dataFrame["1D BTC_ETH"])


	# add new columns with feature labels to the dataframe, for every threshold in percentageTresholdsList.
	# One of these columns will be chosen as feature to predict after a grid search.
	for percentageTreshold in percentageTresholdsList:
		dataFrame["1D " + currencyPairToForecast + " " + str(percentageTreshold)] = (
		dataFrame["1D " + currencyPairToForecast] - percentageTreshold * 0.01).apply(np.sign)
		dataFrame["1D " + currencyPairToForecast + " " + str(percentageTreshold)] = dataFrame[
			"1D " + currencyPairToForecast + " " + str(percentageTreshold)].map({0: -1, -1: -1, 1: 1})
		dataFrame["1D " + currencyPairToForecast + " " + str(percentageTreshold)] = dataFrame[
			"1D " + currencyPairToForecast + " " + str(percentageTreshold)].shift(-1)

	# eliminate the last row because we have one row that don't have indications for increase(+1) or decrease(-1)
	dataFrame = dataFrame[:-1]
	dataFrame.index.name = 'Date'  # give a name to the date column. Here the date coincide with index of the dataframe.
	# Save datafame to a file

	dataFrame.to_csv(input_data_dir_path + "newFinalDataframe_" + period + postfix + ".csv", sep=',', encoding='utf-8')


# print(dataFrame["1D BTC_ETH"])
# TODO I've not eliminated the first rows of the dataframe, that should be full of Nans,
# since it's not possible to calculate n-day return there. Please do this!




# TODO old function, to be deprecated
def setup_dataframe(dataframeClose, dataframeVolume,
					currencyPairToForecast, currencyPairsToTrain, dates, frequency,
					percentageTresholdsList=[0.8, 0.7, 0.6, 0.5],
					returnPeriodList=[1, 2, 3, 7, 14, 48, 144, 336],
					postfix=""):
	# TODO  HERE WE REMOVED A PIECE and inserted the additional arguments dataframeClose, dataframeVolume,

	for returnPeriod in returnPeriodList:
		returnsNPeriods = computeNdaysReturns(dataframeClose, returnPeriod)
		labelsNP = [str(returnPeriod) + "D " + currencyPair for currencyPair in currencyPairsToTrain]
		returnsNPeriods.columns = labelsNP
		returnsNPeriods = returnsNPeriods[returnPeriod:]
		if (returnPeriod == 1):
			# put volume column behind the 1D return
			dataFrame = returnsNPeriods.join(dataframeVolume, how='inner')
		# dataFrameNormalise1D = normalizeData(returnsNPeriods)
		else:
			# returnsNPeriods = normalizeData(returnsNPeriods)
			# put N period return after the volume column
			dataFrame = dataFrame.join(returnsNPeriods, how='inner')

		# join new features: average N day returns
		#	for returnPeriod in returnPeriodList:
		#		# average N day returns
		#		returnsAvgNPeriods = computeNdaysAvgReturns(dataframeClose, returnPeriod)
		#		labelsAvgNPeriods = [str(returnPeriod) + "D " + "Avg_" + currencyPair for currencyPair in currencyPairsToTrain]
		#		returnsAvgNPeriods.columns = labelsAvgNPeriods
		#		returnsAvgNPeriods = returnsAvgNPeriods[returnPeriod:]

		# put average N period return in the dataframe
		#		dataFrame = dataFrame.join(returnsAvgNPeriods, how='inner')

	# set up target for N period return with certain threshold if the money we made is above threshold we map 1.0
	# if equal or below threshold we set it as -1. For the shift 1 row up. it means that we would like to train
	# the future increase(+1) or decrease (-1) with the current data.
	# eg: base on the time 2017-02-03 03:00:00 we know that in the future it will increase therefore we have +1 on that row

	for percentageTreshold in percentageTresholdsList:
		dataFrame["1D " + currencyPairToForecast + " " + str(percentageTreshold)] = (
		dataFrame["1D " + currencyPairToForecast] - percentageTreshold * 0.01).apply(np.sign)
		dataFrame["1D " + currencyPairToForecast + " " + str(percentageTreshold)] = dataFrame[
			"1D " + currencyPairToForecast + " " + str(percentageTreshold)].map({0: -1, -1: -1, 1: 1})
		dataFrame["1D " + currencyPairToForecast + " " + str(percentageTreshold)] = dataFrame[
			"1D " + currencyPairToForecast + " " + str(percentageTreshold)].shift(-1)

	# for currencyPair in currencyPairsToTrain:
	#	dataFrame["1D " + currencyPair] = dataFrameNormalise1D["1D " + currencyPair]
	# pd.series.to_frame(dataFrame["1D " + currencyPair])
	# dataFrame["1D " + currencyPair] = normalizeData(dataFrame["1D " + currencyPair])
	# eliminate the last row because we have one row that dont have indecation for increase(+1) or decrease(-1)
	dataFrame = dataFrame[:-1]
	dataFrame.index.name = 'Date'  # give a name to the date column. Here the date coincide with index of the dataframe.
	# Save datafame to a file
	dataFrame.to_csv(input_data_dir_path + "savedFinalDataframe_" + frequency + postfix + ".csv", sep=',',
					 encoding='utf-8')


# print(dataFrame["1D BTC_ETH"])

# TODO if possible, remove the dependence of this function with respect to calculate_current_features
def generate_feature_labels(currencyPairs, returnPeriodList):
	'''
	Generate all the possible features, given the currencyPairs. This must be the same order of calculate_current_features !!!
	:param currencyPairs:
	:param returnPeriodList:
	:return: list
	'''
	features_labels = []

	#  volumes
	for currencyPair in currencyPairs:
		features_labels.append("Volume " + currencyPair)

		# compute n-day returns
		for n in returnPeriodList:
			features_labels.append(str(n) + "D " + currencyPair)

	return features_labels


def get_available_symbols():
	'''
	Returns available symbols

	why these ones and not other the other 800+ currencies?
	This comes from intuition and experience with trading.
	We will want to check other coins, though.
	In particular one should adapt this set
	(I'm assuming that putting all the coins is difficult and slow) to the wanted coin.
	:return:
	'''
	# right now the set is good for BTC_ETH, one of the (if not the) best currency to forecast imho.
	symbols = ["USDT_BTC", "BTC_ETH", "BTC_XRP", "BTC_LTC",
			   "BTC_DASH", "BTC_XEM", "BTC_XMR", "BTC_ETC",
			   "USDT_ETH", "USDT_LTC", "USDT_XRP", "USDT_ETC",
			   "ETH_ETC"]
	# symbols.extend(["BTC_ZEC", "USDT_ETH", "USDT_LTC", "USDT_XRP", "USDT_ETC", "ETH_ETC"])
	return symbols


# def split_train_test_data(final_df, ratio, currency_pair_to_forecast):
# 	number_of_rows = final_df.shape[0]
# 	# here we use (ratio*100)% of the data to test
# 	size_of_train_sample = number_of_rows - int(number_of_rows * ratio)
# 	features = final_df.drop(currency_pair_to_forecast, 1)  # remove the column to forecast
# 	X_train = features[:size_of_train_sample]
# 	X_test = features[size_of_train_sample:]
# 	y_train = final_df[currency_pair_to_forecast][:size_of_train_sample]
# 	y_test = final_df[currency_pair_to_forecast][size_of_train_sample:]
#
# 	return X_train, X_test, y_train, y_test


def split_train_validation_test(dataFrame, currencyPairToForecast, datesTraining, datesValidating, datesTesting, period,
								percentageTresholdToTrain=0.7, percentageTresholdsList=[0.7]):
	# X stand for feature and y stand for target
	# eg. we tell algorithm to train X_train and y_train, by saying that many sets of X_train refer to these sets of y_train
	# now I am telling you that we have X_test and what is targe y_test
	# get exact training date from the dataframe because of the shift movement

	startDateDataFrame = dataFrame.index[0]
	# remove NaNs lines at the end of the dataframe
	datesTraining = pd.date_range(startDateDataFrame, datesTraining[-2], freq=period)
	if datesValidating.shape[0] > 2:
		datesValidating = pd.date_range(datesValidating[0], datesValidating[-2], freq=period)

	# Split feature training and feature testing
	y_train = dataFrame.ix[datesTraining]["1D " + currencyPairToForecast + " " + str(percentageTresholdToTrain)]
	y_validation = dataFrame.ix[datesValidating]["1D " + currencyPairToForecast + " " + str(percentageTresholdToTrain)]
	y_test = dataFrame.ix[datesTesting]["1D " + currencyPairToForecast + " " + str(percentageTresholdToTrain)]

	# remove the general increase/decrease indication for Nperiod returns from the training sets
	targetLabelList = []
	for percentageTreshold in percentageTresholdsList:
		targetLabelList.append(
			"1D " + currencyPairToForecast + " " + str(percentageTreshold))  # remove the column to forecast
	features = dataFrame.drop(targetLabelList, 1)

	# Split feature training and feature testing
	X_train = features.ix[datesTraining]
	X_validation = features.ix[datesValidating]
	X_test = features.ix[datesTesting]

	# Remove the summer time change for nan.
	X_train = X_train.dropna()
	y_train = y_train.dropna()

	# print("train \n",X_train)
	# print("valid \n",X_validation)
	# print("test \n",X_test)

	return X_train, X_validation, X_test, y_train, y_validation, y_test


###############################################################################################
# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
###############################################################################################

# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
def get_datatable(symbol):
	"""get the data from the csv file"""
	return pd.read_csv("{}.csv".format(symbol))


# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
def get_plotLabel(dataFrame, customTitle="Stock Prices", ylabell="y", xlabell="x", plotNow=True):
	"""Return the plot for stock indicated by symbol.

	Note: Data for a stock is stored in file: <symbol>.csv
	"""
	plt.plot(dataFrame)
	plt.title(customTitle)
	x = plt.xlabel(xlabell, fontsize=8, color='red')
	y = plt.ylabel(ylabell, fontsize=14, color='blue')
	plt.legend(dataFrame.columns, loc='upper left')  # label with the name of the columns
	plt.xticks(fontsize=6)
	if (plotNow == True):
		plt.show()


# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
def getDataFromCSV(symbols, dates, frequency, how='inner', invertOrder=False, usecols=["Date", "Close"],
				   Input_path=input_data_dir_path + '/raw/'):
	"""get the database with columns from symbols and with indices filtered by dates"""

	# create empty database, but with rows labelled by dates
	# dataframe1 get column for date information
	dataframe1 = pd.DataFrame(index=dates)

	for symbol in symbols:
		dataframe = pd.read_csv(Input_path + frequency + "/" + symbol + ".csv", index_col="Date", parse_dates=True,
								usecols=usecols, na_values=["nan"])
		# dataframe: get specified column with close or volume
		if "Close" in usecols:
			dataframe = dataframe.rename(columns={"Close": "Close " + symbol})

		if "Volume" in usecols:
			dataframe = dataframe.rename(columns={"Volume": "Volume " + symbol})
		# join date and specifile data information together
		dataframe1 = dataframe1.join(dataframe, how=how)
	# notice that the index of the two dataframe must be of the same kind (Ex: one with days and one with days and hours it's wrong!)

	# invert them
	if (invertOrder):
		dataframe1 = dataframe1.reindex(index=dataframe1.index[::-1])

	return dataframe1


# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$


def compute_daily_returns(df):
	'''Compute and Return the daily return values
Notice that the result is expressed as fraction of the initial value.
To ger the percentage change, just multiply by 100'''
	daily_returns = df.copy()
	# compute daily returns for row 1 onwards. Notice that .values is necessary to access the underlying numpy dataframe
	# , otherwise pandas try to match the dataframe by former indices
	daily_returns[1:] = (df[1:] / df[:-1].values) - 1
	daily_returns.ix[0, :] = 0  # set to 0 row 0
	return daily_returns


# print(df[1:], end="\n \n")  # all rows after the first one
# print(df[:-1], end="\n \n")  # all rows apart for the last one


# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
def computeNdaysReturns(df, N):
	'''Compute and Return the N days return values
Notice that the result is expressed as fraction of the initial value.
To ger the percentage change, just multiply by 100'''
	nDays_returns = df.copy()
	# compute daily returns for row 1 onwards. Notice that .values is necessary to access the underlying numpy dataframe
	# , otherwise pandas try to match the dataframe by former indices
	# print(df[N:])

	nDays_returns[N:] = (df[N:] / df[:-N].values) - 1

	for rowIndex in range(N):
		nDays_returns.ix[rowIndex, :] = 0  # set to 0 row labelled by rowIndex

	return nDays_returns


# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
def computeNdaysAvgReturns(df, N):
	'''Compute and Return the N days return values
Notice that the result is expressed as fraction of the initial value.
To ger the percentage change, just multiply by 100'''
	# nDaysAvgReturns= df.copy()
	nDays_returns = df.copy()
	# compute daily returns for row 1 onwards. Notice that .values is necessary to access the underlying numpy dataframe
	# , otherwise pandas try to match the dataframe by former indices
	for i in range(1, N):
		print(i)
		# print(df.shape)
		nDays_returns[i:] = (df[i:] / df[:-i].values) - 1
		nDaysAvgReturns[N:] = nDaysAvgReturns[N:] + nDays_returns[i:]

	for rowIndex in range(N):
		nDaysAvgReturns.ix[rowIndex, :] = 0  # set to 0 row labelled by rowIndex

	return nDaysAvgReturns


# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
def compute_cumulative_returns(df):
	'''Compute and Return the cumulative return values
Notice that the result is expressed as fraction of the initial value.
To ger the percentage change, just multiply by 100'''
	cumulative_return = (df / df.ix[0, :]) - 1  # df.ix[0,:] select the first row
	cumulative_return.ix[0, :] = 0  # set to 0 row 0
	return cumulative_return


# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
def normalizeData(dataFrame):
	# take the first non zero data in the column and normalise in respect to the data in the column
	numberOfRows = dataFrame.shape[0]
	numberOfColums = dataFrame.shape[1]
	for j in range(0, numberOfColums):
		for i in range(0, numberOfRows):
			if dataFrame.ix[i, j] > 0:
				# print i:,j,dataFrame.ix[i,j]
				dataFrame.ix[:, j] = dataFrame.ix[:, j] / np.absolute(dataFrame.ix[i, j])
				break

	return dataFrame
	print("Error occurs we cannot normalise data")
	# return dataFrame/dataFrame.ix[0,:]


# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
def openImage(folderPath, file):
	'''
	Open an image in a given folder.
	 :param folderPath: ex: os.getcwd() for current folder
	:param file: name of the file (with extension, ex: "ciao.png")
	:return:
	'''
	import os
	path = os.path.join(folderPath, file)  # path of the file
	print(path)
	from PIL import Image
	im = Image.open(path)
	im.show()  # physically open the png!


# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
def measure_performance(X, y, clf, show_accuracy=True, show_classification_report=True, show_confusion_matrix=True,
						printToFile=False):
	from sklearn import metrics
	y_pred = clf.predict(X)

	print("##########################", '\n')
	if show_accuracy:
		print("Accuracy:{0:.3f}".format(metrics.accuracy_score(y, y_pred)), "\n")
	if show_classification_report:
		print("Classification report")
		print(metrics.classification_report(y, y_pred), "\n")
	if show_confusion_matrix:
		print("Confusion matrix")
		print(metrics.confusion_matrix(y, y_pred), "\n")
	print("##########################", '\n')

	if printToFile:
		file = open(pathTestName, "a")  # ,  encoding= 'utf-8')

		# print the support
		file.write(str(metrics.precision_recall_fscore_support(y, y_pred)[3]) + " ")
		file.close()


# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
# cross validation adapted for time series
def evaluateTimeSeriesSplit(classifier, X, y, printToFile=False):
	from sklearn.model_selection import TimeSeriesSplit
	from sklearn.cross_validation import cross_val_score

	print("Coefficient of determination on training set: ", classifier.score(X, y))

	# create a time series cross validation
	tspCrossValidator = TimeSeriesSplit(n_splits=5)
	# print(tspCrossValidator.split(X_train))

	scores = cross_val_score(classifier, X, y, cv=tspCrossValidator.split(X))
	print(scores)

	averageScore = sum(scores) / float(len(scores))

	if printToFile:
		file = open(pathTestName, "a")  # ,  encoding= 'utf-8')
		file.write(str(classifier.score(X, y)) + " " + str(averageScore) + " ")
		file.close()


# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
def printInfoOnFile(percentageTreshold, currencyPairToForecast, frequency, startDate, endDate, algo, symbols):
	'''
	Helper function to write on a file
	:param percentageTreshold:
	:param currencyPairToForecast:
	:param frequency:
	:param startDate:
	:param endDate:
	:param algo:
	:param symbols: currency pairs considered
	:return:
	'''
	from datetime import datetime
	file = open(pathTestName, "a")  # , encoding='utf-8')
	file.write(str(100 * percentageTreshold) + " " + currencyPairToForecast + " " + str(frequency)
			   + " " + startDate + " " + endDate + " " + algo + " " + str(symbols) + " " + str(
		datetime.now()) + "\n")
	file.close()


# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

def load_data(csv_filename, input_data_dir_path):
	'''
	:param csv_filename: including .csv at the end.
	:param input_data_dir_path:
	:return:
	'''
	input_dataframe_path = os.path.join(input_data_dir_path, csv_filename)
	return pd.read_csv(input_dataframe_path, index_col="Date", parse_dates=True)


# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

def display_menu(choice_name, options):
	'''
	Displays a menu command line menu and returns the selected option.
	It validates the options, if it does not exist keeps asking the user
	:param options: A list
	:return: The selected option
	'''
	keys = range(len(options))
	print("Please choose one option for %s :" % (choice_name))
	for key in keys:
		print("[ %s ]: %s " % (key, options[key]))
	try:
		chosen_option = int(input())
	except ValueError:
		chosen_option = -1  # to avoid ValueErrors and create a non valid option
	if chosen_option not in keys:
		print("That option is not valid..")
		return display_menu(choice_name, options)
	return chosen_option


# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
#
# def split_train_test_data(final_df, ratio, currency_pair_to_forecast):
#     number_of_rows = final_df.shape[0]
#     # here we use (ratio*100)% of the data to test
#     size_of_train_sample = number_of_rows - int(number_of_rows * ratio)
#     features = final_df.drop(currency_pair_to_forecast, 1)  # remove the column to forecast
#     X_train, X_test, y_train, y_test = (features[:size_of_train_sample],
#                                         features[size_of_train_sample:],
#                                         final_df[currency_pair_to_forecast][:size_of_train_sample],
#                                         final_df[currency_pair_to_forecast][size_of_train_sample:])
#     return X_train, X_test, y_train, y_test

# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

def regression_performance(X, y_true, clf, show_mean_absolute_error=True, show_mean_squared_error=True,
						   show_median_absolute_error=True, show_r2_score=True):
	from sklearn import metrics
	y_pred = clf.predict(X)
	if show_mean_absolute_error:
		print("mean_absolute_error:{0:.4f}".format(metrics.mean_absolute_error(y_true, y_pred)), "\n")
	if show_mean_squared_error:
		print("mean_squared_error:{0:.7f}".format(metrics.mean_squared_error(y_true, y_pred)), "\n")
	if show_median_absolute_error:
		print("median_absolute_error:{0:.4f}".format(metrics.median_absolute_error(y_true, y_pred)), "\n")
	if show_r2_score:
		print("r2_score:{0:.3f}".format(metrics.r2_score(y_true, y_pred)), "\n")


# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
def plotRegression(X_test, y_test, y_predictions, xmin='2017-05-29 06:30:00', xmax='2017-05-30 06:30:00',
				   ymin=-0.15, ymax=0.15, xLabel="date", yLabel="\'Daily\' Returns", pointSize=2.5,
				   line_width=1, title="Decision Tree Regression"):
	# Plot the results
	plt.figure()
	plt.scatter(X_test.index, y_test, c="darkorange", label="Test data", s=pointSize)
	plt.plot(X_test.index, y_predictions, color="cornflowerblue", label="Predictions", linewidth=line_width)
	plt.xlabel(xLabel)
	plt.ylabel(yLabel)
	plt.axis([xmin, xmax, ymin, ymax])
	plt.xticks(rotation=45)
	plt.title(title)
	plt.legend()
	plt.show()


# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
def compareDataFrame(dataFrame1, dataFrame2):
	try:
		assert_frame_equal(dataFrame1, dataFrame2)
		return True
	except:  # appeantly AssertionError doesn't catch all
		return False


# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

class ChartData():
    '''
    Query a json from a given url
    '''
    def __init__(self, currencyPair):
        #Ex: BTC_ETH
        self.currencyPair = currencyPair.upper()  #The method upper() returns a copy of the string in which all case-based characters have been uppercased.

#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    def get_json(self, url):
        connected = False
        connection_try = 0
        connection_attempts = 5
        while connected == False:
            connection_try += 1
            try:
                requestUrl = urllib.request.Request(url, headers={'User-Agent':'Magic Browser'})
                connection = urllib.request.urlopen(requestUrl)
                jsonreq = json.loads(connection.read())
                connected = True
            except urllib.error.HTTPError as error:
                if con_try <= connection_attempts:
                    print(type(error) + '\n' + 'Connection unsuccessful after' + str(connection_try) + 'attempts, trying again...')
                else:
                    print(type(error) + '\n' +'Connection timed out...')
                    jsonreq = None
                    break
        return jsonreq

#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    def get_data(self, period=300, days=200, IwantOnlyDateCloseAndVolume=False, notPrintHMS = False):
        """
            Valid values for candlestick period in seconds are 300, 900, 1800, 7200, 14400, and 86400  period
            """
        baseCurrency = (self.currencyPair).split("_")[0]
        currency = (self.currencyPair).split("_")[1]


        json = self.get_json('https://poloniex.com/public?command=returnChartData&currencyPair=' + baseCurrency +'_' + currency +
                             '&start=' + str(time.time() - 60 * 60 * 24 * days) + '&end=9999999999&period=' + str(
            period))
        if (IwantOnlyDateCloseAndVolume and notPrintHMS):
            #convert to human readable date.
			# Notice the utcfromtimestamp instead of fromtimestamp, since fromtimestamp might fail for past dates if a local timezone had different utc offset
            return {'chart_data': [(datetime.datetime.utcfromtimestamp(i['date']).strftime('%Y-%m-%d'), i['close'], float(i['volume'])) for i in json]}
        else:
            if(IwantOnlyDateCloseAndVolume):
                return {'chart_data': [(datetime.datetime.utcfromtimestamp(i['date']).strftime('%Y-%m-%d-%H:%M:%S'),i['close'], float(i['volume'])) for i in json]}
            else:
                 return {'chart_data': [(i['date'], i['open'], i['close'], i['high'], i['low']) for i in json],
                    'volume_data': [float(i['volume']) for i in json]}
    def getDataRefactored(self, period=300, days=200, IwantOnlyDateCloseAndVolume=False, notPrintHMS = False):
        """
        Valid values for candlestick period in seconds are 300, 900, 1800, 7200, 14400, and 86400  period
        Store the value with the same nested dictionary
        """

def getDataFromPolonex(currentTime, poloRawData, currencyPair):

    # load recent close price from polonix
    closePrice = poloRawData[currencyPair]['last']
    quoteVolume = poloRawData[currencyPair]['quoteVolume']
    # format 2017-10-07 14:54:43
    # UTC time
    mostRecentDate = datetime.datetime.utcfromtimestamp(currentTime).strftime('%Y-%m-%d-%H:%M:%S')
    return [(mostRecentDate, closePrice, quoteVolume)]


#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
def saveToCSV(data, filename):
	'''

	:param data: list with "Date", "Close", "Volume"
	:param filename: for instance ciao.csv
	:return:
	'''
	import csv
	import os

	path = os.path.join(os.getcwd(), filename)  # path of the file
	with open(path, "w") as csv_file:
		writer = csv.writer(csv_file, delimiter=',')
		writer.writerow(("Date", "Close", "Volume"))
		for line in data:
			writer.writerow(line)


# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$


def saveAllcurrencyPairs(currencyPairs, period, sampleFrequency, days):
    '''

    :param currencyPairs: list
    :param period: allowed period, in seconds, for instance 300
    :param sampleFrequency: for instance, "5min"
    :param days: integer number, for instance 200
    :return:
    '''
    for currencyPair in currencyPairs:
        data = ChartData(currencyPair).get_data(period=period, days=days, IwantOnlyDateCloseAndVolume=True, notPrintHMS=False)['chart_data']
        saveToCSV(data, "input/raw/" + sampleFrequency + "/" + currencyPair + ".csv")
        print(currencyPair," completed")
# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

def saveAllcurrencyPairsRefactored(currencyPairs, periodInSeconds, sampleFrequency, days):
    # init polonix object
    # current unix time (pay attention, it is not the utc time!)
    now = time.time()
    # time window for how manydays of data to download
    timeWindow = days * 60 * 60 * 24 / periodInSeconds
    # init polonix object
    polo = poloniex3.Poloniex3()
    # store the dictData for the number of rows needed
    dictData = {}

    # If this happens in the future, we will need to query the data in a more complicated way,
    # maybe using coach or putting a 1 second sleep
    for currencyPair in currencyPairs:
        dictData[currencyPair] = polo.returnChartData(currencyPair=currencyPair, period=periodInSeconds,
            start=now - timeWindow)  # end is now, by default (if not specified)
    print(dictData["BTC_ETH"])
# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
def saveAllcurrencyPairsSub5min(periodInSeconds, sampleFrequency, maxFuturePeriods):
    '''
    :param periodInSeconds: allowed period, in seconds, for instance 300
    :param sampleFrequency: for instance, "5min"
    :param maxFuturePeriods: integer number, for instance 200
    :return:
    '''
    # TODO: add while loop a signal to stop running
    # init polonix object
    polo = poloniex3.Poloniex3()
    dictData={}
    for periodIndex in range(0,maxFuturePeriods):
        # current unix time (pay attention, it is not the utc time!)
        queryTime = time.time()
        poloRawData = polo.returnTicker()
        utcTime = datetime.datetime.utcfromtimestamp(queryTime).strftime('%Y-%m-%d-%H:%M:%S')
        dictData[utcTime] = pd.DataFrame.from_dict(poloRawData, orient='index').T
        finishTime = time.time()
        diffTime = finishTime - queryTime
        # delay for 30s
        time.sleep(periodInSeconds - diffTime)
        print(utcTime)
    dataFrame = pd.concat(dictData)
    # selecting column dataFrame["BTC_ARDR"]
    # selecting index dataFrame.loc[utcTime,'id']
    dataFrame.to_csv("input/raw/" + sampleFrequency + "/" + "test" + ".csv", sep=',', encoding='utf-8')
    # read dataFrame2 from dataFrame file
    dataFrame2 = pd.read_csv("input/raw/" + sampleFrequency + "/" + "test" + ".csv", index_col=[0,1],dtype=object)
# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

def gain(closingPrice, openingPrice):
	'''
	Compute the profit (if openingPrice < closingPrice) or the loss.
	If one wants the percentage, just multiply by 100.
	:param openingPrice:
	:param closingPrice:
	:return gain:
	'''
	return (float(closingPrice) - float(openingPrice)) / float(openingPrice)


#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
def featureImportance(classifier,labels):
    '''
    List the importance of the features, as computed by the classifier.
    :param classifier:
    :param labels: features labels
    :return:
    '''
    print("Feature Importance : \n")
    print(pd.DataFrame(list(zip(labels, np.transpose(classifier.feature_importances_)))).sort_values(1, ascending=False),'\n')
# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
def roundTime(dt=None, roundTo=60, to='up'):
    """
    Round a datetime object to a multiple of a timedelta

    dt :      datetime.datetime object, default now.
    roundTo : Closest number of seconds to roundup to, default 1 minute.
    to:       'up' round up to certain second 'down' round down to certain second, '
              average' bigger than middle value round up other wise rounddown
    """
    if dt is None:
        dt = datetime.now()
    seconds = (dt - dt.min).seconds
    # round up & round down
    if to == 'up':
        rounding = (seconds + roundTo) // roundTo * roundTo
    elif to == 'down':
        rounding = seconds // roundTo * roundTo
    else:
        rounding = (seconds + roundTo / 2) // roundTo * roundTo
    return dt + timedelta(0, rounding - seconds, -dt.microsecond)
# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
def get_path(pathFromCurrentDirectory):
	import os
	current_dir_path = os.path.dirname(os.path.realpath(__file__))
	dir_path = os.path.join(current_dir_path, pathFromCurrentDirectory)
	# print(dir_path)
	return str(dir_path)


# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
def resample_data(X, y, endDateTraining,  resample=False, resample_method = 0):
	'''
	Resample X and y. This will in general change the shape of both X and y.
	:param X: Dataframe with features, indexed by dates.
	:param y: Series with labels, indexed by dates.
	:return: Resampled X,y.
	'''

	print("1 labels:" , list(y).count(1))
	print("-1 labels:" , list(y).count(-1))


	if (resample == False):
		return X, y
	else:
		from sklearn.linear_model import LogisticRegression

		from imblearn.over_sampling import RandomOverSampler
		from imblearn.over_sampling import ADASYN
		from imblearn.over_sampling import SMOTE

		from imblearn.under_sampling import ClusterCentroids
		from imblearn.under_sampling import RandomUnderSampler
		from imblearn.under_sampling import NearMiss
		from imblearn.under_sampling import EditedNearestNeighbours
		from imblearn.under_sampling import RepeatedEditedNearestNeighbours
		from imblearn.under_sampling import AllKNN
		from imblearn.under_sampling import CondensedNearestNeighbour
		from imblearn.under_sampling import OneSidedSelection
		from imblearn.under_sampling import NeighbourhoodCleaningRule
		from imblearn.under_sampling import InstanceHardnessThreshold

		from imblearn.combine import SMOTEENN
		from imblearn.combine import SMOTETomek

		from imblearn.ensemble import EasyEnsemble
		from imblearn.ensemble import BalanceCascade

		ratio = {1: (list(y).count(1) + 500 ), -1: list(y).count(-1)}

		samplers = [RandomOverSampler(ratio = ratio , random_state=0)
			, SMOTE(ratio = ratio)
			, ADASYN(ratio = ratio)
			, ClusterCentroids(random_state=0)
			, RandomUnderSampler(random_state=0)
			, NearMiss(random_state=0, version=1)
			, EditedNearestNeighbours(random_state=0)
			, RepeatedEditedNearestNeighbours(random_state=0)
			, AllKNN(random_state=0)
			, CondensedNearestNeighbour(random_state=0)
			, OneSidedSelection(random_state=0)
			, NeighbourhoodCleaningRule(random_state=0)
			, InstanceHardnessThreshold(random_state=0, estimator=LogisticRegression())
			, SMOTEENN()
			, SMOTETomek(random_state=0)
			, EasyEnsemble(random_state=0, n_subsets=10)
			, BalanceCascade(random_state=0, estimator=LogisticRegression(random_state=0), n_max_subset=4)
					]

		columns_x = X.columns
		indices = X.index
		shape_before = X.shape[0]

		sampler = samplers[resample_method]
		print('\n', sampler, '\n')

		new_X, new_y = sampler.fit_sample(X.values, np.array(y))
		shape_after = new_X.shape[0]

		# here the dates indices don't have an actual meaning, for instance data augmentation will give dates in the future
		# it is not obvious that this is the correct way to handle the situation
		if (shape_after > shape_before):
			# append new indices
			indices2 = pd.date_range(endDateTraining, periods=(shape_after - shape_before), dtype='datetime64[ns]',freq='5T')
			indices = indices.union(indices2)
		else:
			indices = pd.date_range(endDateTraining, periods=shape_after, dtype='datetime64[ns]',freq='5T')

		new_X = pd.DataFrame(data=new_X, index=indices, columns=columns_x)
		new_y = pd.Series(new_y, index=indices)

		return new_X, new_y

# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
def utc2LocalTime(timeUTC):
    from_zone = tz.tzutc()
    to_zone = tz.tzlocal()
    timeUTC = timeUTC.replace(tzinfo=from_zone)
    timeLocal = timeUTC.astimezone(to_zone)
    return timeLocal

def datetime2string(utcTime):
    utcTimestamp = utcTime.timestamp()
    return datetime.fromtimestamp(utcTimestamp).strftime('%Y-%m-%d-%H:%M:%S')

def datetime2DayString(utcTime):
    utcTimestamp = utcTime.timestamp()
    return datetime.fromtimestamp(utcTimestamp).strftime('%Y-%m-%d')

# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
# calculate abitary time interval
def calculateVolumeTradHist(currencyPair, periodInSeconds, utcTime):
    # wrapper from utc to local time
    startDateTime = utc2LocalTime(utcTime)
    # local time to unix time
    startUnixTime = startDateTime.timestamp()
    tradeList = polo.marketTradeHist(currencyPair, start=startUnixTime-periodInSeconds, end=startUnixTime)
    print("trading length: ", len(tradeList))
    amounts = []
    for i in range(len(tradeList)):
        trade_type = tradeList[i]['type']
        if  trade_type == 'buy':
            amount = float(tradeList[i]['amount'])
            amounts.append(amount)
        if  trade_type == 'sell':
            amount = float(tradeList[i]['amount'])
            amounts.append(amount)
    return sum(amounts)
def return5minVolume(numePeriods, dataFrameRead, currencyPair,):
    reshaped = dataFrameRead[currencyPair]["volume"][:].values.reshape((-1,numePeriods))
    return np.sum(reshaped, axis = 1)
def return5minClosePrice(numePeriods, dataFrameRead, currencyPair):
    closePriceList = dataFrameRead[currencyPair]["close"][:].values.reshape((-1,numePeriods))
    return closePriceList[:,-1]
# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
# calculate abitary time interval
def calculateVolumeGetChart(currencyPair, periodInSeconds, startDateTime, lengthInSeconds):
    chartHistory = wrapperReturnChartData(currencyPair, periodInSeconds, startDateTime, lengthInSeconds)
#     print ("yesterday 5 min trading length: ",len(chartHistory))
    amounts = []
    for i in range(len(chartHistory)):
        amounts.append(float(chartHistory[i]['quoteVolume']))
    return sum(amounts)
# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
def getCloseVolumeTradeHist(currencyPair, previousClosePrice, periodInSeconds, utcTime):
    # wrapper from utc to local time
    startDateTime = utc2LocalTime(utcTime)
    # local time to unix time
    startUnixTime = startDateTime.timestamp()

    tradeList = polo.marketTradeHist(currencyPair,
                                     start=startUnixTime-periodInSeconds,
                                     end=startUnixTime)
    amounts = []
    for i in range(len(tradeList)):
        trade_type = tradeList[i]['type']
        if  trade_type == 'buy':
            amount = float(tradeList[i]['amount'])
            amounts.append(amount)
        if  trade_type == 'sell':
            amount = float(tradeList[i]['amount'])
            amounts.append(amount)
    if (len(tradeList) == 0):
        return {'close': previousClosePrice, 'volume': 0.0}
    else:
        return {'close': tradeList[0]['rate'], 'volume': sum(amounts)}
# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
def saveAllcurrencyPairsSub5minRefactored(currencyPairs, beginTime, endTime, periodInSeconds,
    sampleFrequency, maxPeriods, saveTimePeriods):
    '''
    notice: volume of time is stored based on PREVIOUS period!!!
    '''
    tradeHistData = {}
    previousClosePrice = {}
    for currencyPair in currencyPairs:
        previousClosePrice[currencyPair] = 0
    diagnosticTime = 120

    for days in range(0, int(maxPeriods/saveTimePeriods)):
        dictData = {}
        for periodIndex in range(0,int(saveTimePeriods)):
            queryTime = beginTime
            # ===================================================
            # get hist data and build data frame
            # ===================================================
            dateTimeToStore = datetime2string(beginTime)
            # store data according to currency pairs
            for currencyPair in currencyPairs:
                tradeHistData[currencyPair] = getCloseVolumeTradeHist(currencyPair,
                                                                      previousClosePrice[currencyPair],
                                                                      periodInSeconds,
                                                                      beginTime)
                previousClosePrice[currencyPair] = tradeHistData[currencyPair]['close']
            reformTradeHistData = {(outerKey, innerKey): values
                for outerKey, innerDict in tradeHistData.items()
                for innerKey, values in innerDict.items()}
            dictData[dateTimeToStore] = reformTradeHistData

            if (periodIndex%diagnosticTime == 0):
                print("================================================================")
                print(" Iteration at: " + str(periodIndex))
                print(" Time at: " + str(beginTime))
                print("================================================================")
            beginTime = beginTime + timedelta(seconds=periodInSeconds)
        dataFrame = pd.DataFrame(dictData).T
        print(dataFrame)
        if (days > 0):
            with open("input/raw/" + sampleFrequency + "/" + "2017-05-01BTC_ETH_USDT2017-11-06.csv", 'a') as f:
                dataFrame.to_csv(f, sep=',', encoding='utf-8', header = False)
        else:
            with open("input/raw/" + sampleFrequency + "/" + "2017-05-01BTC_ETH_USDT2017-11-06.csv", 'a') as f:
                dataFrame.to_csv(f, sep=',', encoding='utf-8', header = False)
        # dataFrame.to_csv("input/raw/" + sampleFrequency + "/" +
        #     datetime2DayString(beginTime) + "BTC_ETH_USDT" + datetime2DayString(endTime)+ ".csv",
        #      sep='\t', encoding='utf-8')




















def utcTime_2_unixTime(utcTime):
    localTime = utc2LocalTime(utcTime)
    unixTime = localTime.timestamp()
    return unixTime


def get_trade_history_data(currencyPairs, sampleFrequency, utcTime):
    tradeHistoryDataFrame = {}
    tradeHisotryDict = {}
    unixTime = utc_2_unix_time(utcTime)

    for currencyPair in currencyPairs:
        tradeHistoryDict[currencyPair] = polo.marketTradeHist(currencyPair,
                                                              start=unixTime-sampleFrequency,
                                                              end=unixTime)

    tradeHistoryData['close'] = max(tradeList[0]['rate'], 0)

    buyAmountList = []
    sellAmountList = []

    for trade in tradeList:
        tradeType = trade['type']
        amount = float(trade['amount'])

        if tradeType == 'buy':
            buyAmountList.append(amount)
        else:
            sellAmountList.append(amount)

    tradeHistoryData['volume'] = sum(buyAmountList) + sum(sellAmountList)
    tradeHistoryData['buy_volume'] = sum(buyAmountList)
    tradeHistoryData['sell_volume'] = sum(sellAmountList)

    return tradeHistoryData


def create_data_set(currencyPairs, dateRange, writeTime):
    tradeHistoryDataFrame = pd.DataFrame(index=dateRange)

    for time in dateRange:
        # store data according to currency pairs
        tradeHistoryDataFrame.loc[time] = get_trade_history_data(currencyPairs,
                                                                 dateRange.freq,
                                                                 time)

        if (time%writeTime == 0):
            print("============================================================")
            print(time, " - saving data")
            print("============================================================")

        dataFrame = pd.DataFrame(dictData).T

        filename = "./input/data_" + sampleFrequency + ".csv"

        with open(filename, 'a') as file:
            dataFrame.to_csv(file, sep=',', encoding='utf-8', header = False)
