import os
import pandas as pd
import numpy as np
import sys

sys.path.insert(1, '.')
import classifier as clf
import time

inputDirectory = './input/poloniex/'

if __name__ == "__main__":
    startDate = pd.Timestamp('2017-10-01-00:00:00')
    endDate = pd.Timestamp('2017-11-23-02:00:00')

    parameters = {'targetCurrencyPairList': ["BTC_ETH"],
                  'featureCurrencyPairList': ["BTC_ETH", "USDT_BTC", "USDT_ETH"],
                  'samplingFrequency':'300s',
                  'quantityList':['close', 'volume'],
                  'tradingPeriodList':['5min'],
                  'thresholdList':[0.007],
                  'nWindowsCloseList':[],
                  'nWindowsVolumeList':[],
                  'nPeriodReturnCloseList':[1, 2, 3, 4, 5, 6, 7, 8, 9,
                                            10, 20, 40, 60, 80],
                  'nPeriodReturnVolumeList':[1, 2, 3, 4, 5, 6, 7, 8, 9,
                                             10, 20, 40, 60, 80],
                  'nPeriodReturnMovAvCloseList':[],
                  'nPeriodReturnMovAvVolumeList':[]}


    # startDate = '2017-09-01-00:00:00'

    # parameters = {'targetCurrencyPairList': ["BTC_ETH"],
    #           'featureCurrencyPairList': ["BTC_ETH", "USDT_BTC", "USDT_ETH"],
    #           'samplingFrequency':'30s',
    #           'quantityList':['close', 'volume'],
    #           'tradingPeriodList':['5min'],
    #           'thresholdList':[0.007],
    #           'nWindowsCloseList':[2],
    #           'nWindowsVolumeList':[2],
    #           'nPeriodReturnCloseList':[4],
    #           'nPeriodReturnVolumeList':[4],
    #           'nPeriodReturnMovAvCloseList':[100],
    #           'nPeriodReturnMovAvVolumeList':[100]}

    classifier = clf.Classifier(**parameters)

    X_columns = pd.MultiIndex(levels=[['date'], ['']], labels=[[]]*2, names=['date',''])
    X = pd.DataFrame(columns=X_columns)

    for currencyPair in parameters['featureCurrencyPairList']:
        print('Reading data file for', currencyPair)
        filename = currencyPair + '.csv'
        X_currencyPair = pd.read_csv(inputDirectory+filename, header=[0, 1], index_col=[0])
        X_currencyPair.index = pd.to_datetime(X_currencyPair.index)
        X_currencyPair = X_currencyPair[startDate:endDate]
        print('Done reading!')

        X_currencyPair['date', ''] = X_currencyPair.index
        X_currencyPair.index = pd.Index(X_currencyPair[currencyPair, 'globalTradeID'],
                                        name = 'globalTradeID')
        X_currencyPair.drop('globalTradeID', axis=1, level=1, inplace=True)
        X = pd.merge(X, X_currencyPair, on='date', left_index=True, right_index=True, how='outer')

    X.set_index('date', inplace=True)

    classifier.fit(X)

    filename = 'fittedClassifier.pkl'
    print('Saving fitted classifier in: ', filename)
    classifier.save(filename)
