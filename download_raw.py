import os
import numpy as np
import pandas as pd
import sys
import utils
from datetime import datetime
from datetime import timedelta

setupSub5min = False
setupSub5minRefactored = True
refactored = False

if setupSub5min:
	# major function begin
	sampleFrequency = "5sec"
	periodInSeconds = 5
	maxFuturePeriods = 200
	savingDirectory = 'input/' + 'raw/' + sampleFrequency
	if not os.path.exists(savingDirectory):
		os.mkdir(savingDirectory)
	utils.saveAllcurrencyPairsSub5min(periodInSeconds,sampleFrequency,maxFuturePeriods)
elif setupSub5minRefactored:
	Nday = 50
	secondsInday = 24*60*60
	totalPeriodInSeconds = Nday*secondsInday
	periodInSeconds = 30
	sampleFrequency = "30sec"
	currencyPairs = ["USDT_BTC","BTC_ETH","USDT_ETH"]

	timeNow = datetime.utcnow()
	# begin time e.g. 2017 september 1st 00:00:00 utc time
	beginTime = datetime(2017, 11, 21, 0, 0, 0, 0)
	# end time now
	endTime = utils.roundTime(timeNow, roundTo=periodInSeconds,to='down')
	saveTimePeriods = 1*secondsInday/periodInSeconds
	maxPeriods = (endTime - beginTime)/timedelta(seconds=periodInSeconds)
	savingDirectory = 'input/' + 'raw/' + sampleFrequency
	if not os.path.exists(savingDirectory):
		os.mkdir(savingDirectory)
	utils.saveAllcurrencyPairsSub5minRefactored(currencyPairs, beginTime, endTime, periodInSeconds,
		sampleFrequency, maxPeriods, saveTimePeriods)
elif refactored:
	sampleFrequency = "5min"
	timeFrequencies = {"5min":300,"15min": 900,"30min": 1800,"2hours": 7200,"4hours": 14400,"1day":  86400}
	currencyPairs = ["USDT_BTC","BTC_ETH","USDT_ETH"]
	currencyPairs.extend(["BTC_XRP","BTC_LTC","BTC_DASH","BTC_ETC","BTC_XEM","BTC_XMR","BTC_ZEC" ,
		"USDT_LTC", "USDT_XRP", "USDT_ETC", "ETH_ETC"])
	timeRangeInDays =1
	utils.saveAllcurrencyPairsRefactored(currencyPairs, timeFrequencies[sampleFrequency], sampleFrequency, timeRangeInDays)
else:
	# saveToCSV(data, "input_data/raw/BTC_LTC_new.csv")
	sampleFrequency = "5min"
	timeFrequencies = {"5min":300,"15min": 900,"30min": 1800,"2hours": 7200,"4hours": 14400,"1day":  86400}

	currencyPairs = ["USDT_BTC","BTC_ETH","USDT_ETH"]
	currencyPairs.extend(["BTC_XRP","BTC_LTC","BTC_DASH","BTC_ETC","BTC_XEM","BTC_XMR","BTC_ZEC" , "USDT_LTC", "USDT_XRP", "USDT_ETC", "ETH_ETC"])

	#data = ChartData()
	utils.sampleFrequency = "5min"
	timeRangeInDays = 280
	utils.saveAllcurrencyPairs(currencyPairs,timeFrequencies[sampleFrequency],sampleFrequency,timeRangeInDays)
