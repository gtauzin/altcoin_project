import utils

def generate_or_load_dataframe(currencyPairToForecast, currencyPairsToTrain, dates, period,input_data_dir_path,
							   percentageTresholdsList,
							   returnPeriodList,
							   GENERATE_DATA_FRAME = False):

	#
	if GENERATE_DATA_FRAME:
		# Get dataframe close
		dataFrameRAWClose = utils.getDataFromCSV(currencyPairsToTrain, dates, period, how='inner',
												 usecols=["Date", "Close"],
												 Input_path=input_data_dir_path + '/raw/')

		# print(dataFrameRAWClose)
		dataFrameRAWClose = utils.normalizeData(dataFrameRAWClose)
		# Get dataframe volumes
		dataframeRAWVolume = utils.getDataFromCSV(currencyPairsToTrain, dates, period, how='inner',
												  usecols=["Date", "Volume"], Input_path=input_data_dir_path + '/raw/')
		dataframeRAWVolume = utils.normalizeData(dataframeRAWVolume)

		# build the dataframe
		utils.setup_dataframe(dataFrameRAWClose, dataframeRAWVolume,
							  currencyPairToForecast, currencyPairsToTrain,
							  dates, period, percentageTresholdsList,
							  returnPeriodList, postfix="botTestGeneralised")

	# TODO
	# utils.setup_dataframe_refactored(dataFrameRAWClose, dataframeRAWVolume,
	#					  currencyPairToForecast, currencyPairsToTrain,
	#					  dates, period, percentageTresholdsList,
	#					  returnPeriodList, postfix="botTestRefactored")


	# load the dataframe
	csv_filename = "savedFinalDataframe_" + period + "botTestGeneralised" + ".csv"
	dataFrame = utils.load_data(csv_filename, input_data_dir_path)



	# TODO manually reorder the columns to be the same as the refactored one.
	# TODO remove this when dataframe_refactored is ready
	#dataFrame = dataFrame[['Volume BTC_ETH', '1D BTC_ETH', '2D BTC_ETH', '3D BTC_ETH',
	#					   'Volume BTC_LTC', '1D BTC_LTC', '2D BTC_LTC', '3D BTC_LTC',
	#					   '1D BTC_ETH 0.7', '1D BTC_ETH 0.6', '1D BTC_ETH 0.5']]
	nameLabels = utils.generate_feature_labels(currencyPairsToTrain, returnPeriodList)
	nameLabels.extend(['1D BTC_ETH 0.7', '1D BTC_ETH 0.6', '1D BTC_ETH 0.5'])
	#print(nameLabels)
	dataFrame = dataFrame[nameLabels]

	# TODO
	# dataFrame_refactored = utils.load_data("newFinalDataframe_"+ period + "botTestRefactored"+ ".csv", input_data_dir_path)

	print(dataFrame)

	return dataFrame
