## Docs 

Here you can find notes on the models as well as references. 
Feel free to create multiple folders or other markdown files.


## Legacy Stuff

All the legacy code from **classification** has been put in a folder `/legacy`.

* **HistoScatterPlot**: Play with histograms and scatterPlot for the daily returns of some currenciesPair.
Compute the correlation between various pairs (cool confirmation: USDT_BTC is anti-correlated with BTC_altcoins)

* **Altcoin1**: Machine Learning algos applied to daily returns

* **Altcoin2**: Machine Learning algos applied to daily returns, n-day returns (3 and 7 for instance) and daily volumes.

* **Altcoin3**: Same as before, but with different time lapse and saving dataframe to csv.
