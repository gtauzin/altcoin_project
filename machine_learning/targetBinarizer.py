import numpy as np

from sklearn.preprocessing import Binarizer

from sklearn.utils.validation import check_X_y, check_array, check_is_fitted
from sklearn.utils.multiclass import unique_labels

class TargetBinarizer(Binarizer):
    """ An example transformer that returns the element-wise square root..

    Parameters
    ----------
    demo_param : str, optional
        A parameter used for demonstation of how to pass and store paramters.

    Attributes
    ----------
    input_shape : tuple
        The shape the data passed to :meth:`fit`
    """
    def __init__(self, threshold=0.007, copy=False, periods=10):
        super(TargetBinarizer, self).__init__(threshold, copy)
        self.periods = periods

    def fit(self, X, y=None):
        """A reference implementation of a fitting function for a transformer.

        Parameters
        ----------
        X : array-like or sparse matrix of shape = [n_samples, n_features]
            The training input samples.
        y : None
            There is no need of a target in a transformer, yet the pipeline API
            requires this parameter.

        Returns
        -------
        self : object
            Returns self.
        """
        #X = check_array(X)

        #self.input_shape_ = X.shape

        # Return the transformer
        return self

    def transform(self, X):
        """ A reference implementation of a transform function.

        Parameters
        ----------
        X : array-like of shape = [n_samples, n_features]
            The input samples.

        Returns
        -------
        X_transformed : array of int of shape = [n_samples, n_features]
            The array containing the element-wise square roots of the values
            in `X`
        """
        # Check is fit had been called
        #check_is_fitted(self, ['input_shape_'])

        # Input validation
        #X = check_array(X)

        # Check that the input is of the same shape as the one passed
        # during fit.
        # if X.shape != self.input_shape_:
        #     raise ValueError('Shape of input is different from what was seen'
        #                      'in `fit`')
        levels = [level+'_threshold_'+str(self.threshold) for level in X.columns.get_level_values(1)]
        X.columns.set_levels(levels, level=1, inplace=True)
        X[:] = super(TargetBinarizer, self).transform(X)
        X=X.shift(periods=-self.periods, axis=0)

        return X
