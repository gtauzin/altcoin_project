import numpy as np
import pandas as pd
import sklearn as sk

import dataSampler as dS
import nPeriodsReturner as nPR
import firstLinesDropper as fLD
import targetBinarizer as tB
import lastLinesDropper as lLD

from sklearn.pipeline import Pipeline
from pandasFeatureUnion import PandasFeatureUnion as FeatureUnion

import sklearn.preprocessing as prep

class TargetPipeline(Pipeline):
    """ A transformer that returns the N periods returns of an input quantity

    Parameters
    ----------
    quantity : str
        Quantity to calculate the N periods return of
    nPeriodsList : list of int
        List of number of periods

    Attributes
    ----------
    inputShape : tuple
        The shape the data passed to :meth:`fit`
    """

    def __init__(self, currencyPairList=['BTC_ETH'], samplingFrequency='30s',
                 tradingPeriodList=['5min'], thresholdList=[0.007]):
        self.currencyPairList = currencyPairList
        self.samplingFrequency = samplingFrequency
        self.tradingPeriodList = tradingPeriodList
        self.thresholdList = thresholdList
        self.nPeriodMax = max([int(pd.Timedelta(tradingPeriod)/pd.Timedelta(self.samplingFrequency)) for tradingPeriod in self.tradingPeriodList])

        steps = []

        steps.append(('dataSampling',
                      dS.DataSampler(currencyPairList=self.currencyPairList,
                                     samplingFrequency=self.samplingFrequency,
                                     quantityList=['close'])))

        unionSteps = []
        self.levelList = []
        self.labelList = []
        for tradingPeriod in self.tradingPeriodList:
            for threshold in self.thresholdList:
                level = 'target_'+tradingPeriod+'_'+str(threshold)
                self.levelList.append(level)
                self.labelList.append(len(self.labelList))
                unionSteps.append(self.union_step(level, tradingPeriod, threshold))

        steps.append(('union', FeatureUnion(unionSteps)))

        super(TargetPipeline, self).__init__(steps)

    def union_step(self, level, tradingPeriod, threshold):
        nPeriod = int(pd.Timedelta(tradingPeriod)/pd.Timedelta(self.samplingFrequency))
        steps = []

        steps.append(('nPeriodReturn',
                      nPR.NPeriodsReturner(quantityList=['close'],
                                           nPeriodsList=[nPeriod])))
        steps.append(('dropFirstLines',
                      fLD.FirstLinesDropper(self.nPeriodMax)))
        steps.append(('binarizer', tB.TargetBinarizer(threshold=threshold,
                                                      periods=self.nPeriodMax)))
        steps.append(('dropLastLines',
                      lLD.LastLinesDropper(self.nPeriodMax)))
        return (level, Pipeline(steps))
