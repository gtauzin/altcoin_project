import numpy as np
import pandas as pd
import sklearn as sk

from sklearn.utils.validation import check_X_y, check_array, check_is_fitted
from sklearn.base import BaseEstimator, TransformerMixin

class NPeriodsReturner(BaseEstimator, TransformerMixin):
    """ A transformer that returns the N periods returns of an input quantity

    Parameters
    ----------
    quantityList : list of str
        List of quantity to calculate the N periods return of
    nPeriodsList : list of int
        List of number of periods

    Attributes
    ----------
    inputShape : tuple
        The shape the data passed to :meth:`fit`
    """

    def __init__(self, quantityList = ['close'], nPeriodsList=[1]):
        self.quantityList = quantityList
        self.nPeriodsList = nPeriodsList

    def get_params(self, deep=True):
        return {'quantityList': self.quantityList,
                'nPeriodsList': self.nPeriodsList}

    def set_params(self, **parameters):
        for parameter, value in parameters.items():
            self.setattr(parameter, value)
        return self

    def fit(self, X, y=None):
        """A reference implementation of a fitting function for a transformer.

        Parameters
        ----------
        X : array-like or sparse matrix of shape = [n_samples, n_features]
            The training input samples.
        y : None
            There is no need of a target in a transformer, yet the pipeline API
            requires this parameter.

        Returns
        -------
        self : object
            Returns self.
        """
        #check_array(X)

        #self.inputShape = X.shape

        # Return the transformer
        return self

    def transform(self, X):
        """ Implementation of the transform function that returns the N periods
        returns of a quantity

        Parameters
        ----------
        X : array-like of shape = [n_samples, n_features]
            The input samples.

        Returns
        -------
        X_transformed : array of int of shape = [n_samples, n_features]
            The array containing the N periods returns of the input quantity
            in `X`
        """
        # Check is fit had been called
        #check_is_fitted(self, ['inputShape'])

        # Input validation
        #check_array(X)

        # Check that the input is of the same shape as the one passed
        # during fit.
        # if X.shape != self.inputShape:
        #     raise ValueError('Shape of input is different from what was seen'
        #                      'in `fit`')

        X_transformed_columns = pd.MultiIndex(levels=[[]]*2, labels=[[]]*2, names=["",""])
        X_transformed = pd.DataFrame(index=X.index,columns=X_transformed_columns)

        quantityListColumnMask = np.isin(X.columns.get_level_values(1), self.quantityList)
        X = X.loc[:, quantityListColumnMask]

        for n in self.nPeriodsList:
            nPeriodReturn = X.pct_change(periods=n)
            levels = [quantity+"_return_"+str(n) for quantity in self.quantityList]
            labels = [X.columns.labels[0], [0] * X.columns.labels[0]]
            nPeriodReturn.columns.set_labels(labels, inplace=True)
            nPeriodReturn.columns.set_levels(levels, level=1, inplace=True)
            X_transformed = pd.concat([X_transformed, nPeriodReturn], axis=1)
            del nPeriodReturn

        return X_transformed
