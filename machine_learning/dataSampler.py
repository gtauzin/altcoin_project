import numpy as np
import pandas as pd
import sklearn as sk
from pandas.core.resample import Resampler as rsp
from sklearn.utils.validation import check_X_y, check_array, check_is_fitted
from sklearn.base import BaseEstimator, TransformerMixin

class DataSampler(BaseEstimator, TransformerMixin):
    """
    data sampling transformer that returns a sampled Pandas dataframe with a datetime index

    Parameters
    ----------
    nPeriodsList : list of int
        List of number of periods

    Attributes
    ----------
    inputShape : tuple
        The shape the data passed to :meth:`fit`
    """

    #TODO: fillna for rate based quantities should return the previous quantity + small value
    quantityRecipeDict = {'open': ['rate', rsp.first, 'all', None, 'ffill'],
                          'close': ['rate', rsp.last, 'all', None, 'ffill'],
                          'high': ['rate', rsp.max, 'all', None, 'ffill'],
                          'low': ['rate', rsp.min, 'all', None, 'ffill'],
                          'average': ['rate', rsp.mean, 'all', None, 'ffill'],
                          'volume_buy': ['amount', rsp.sum, 'buy', 1e-8, None],
                          'volume_sell': ['amount', rsp.sum, 'sell', 1e-8, None],
                          'volume': ['amount', rsp.sum, 'all', 1e-8, None]}

    def __init__(self, currencyPairList=['BTC_ETH'], samplingFrequency='30s',
                 quantityList=['close', 'volume']):
        self.currencyPairList = currencyPairList
        self.samplingFrequency = samplingFrequency
        if set(quantityList).issubset(set(self.quantityRecipeDict.keys())):
            self.quantityList = quantityList
        else:
            raise ValueError('One of the listed quantity is not supported.')

    def get_params(self, deep=True):
        return {'currencyPairList': self.currencyPairList,
                'sampingPeriod': self.samplingFrequency,
                'quantityList': self.quantityList}

    def set_params(self, **parameters):
        for parameter, value in parameters.items():
            self.setattr(parameter, value)
        return self

    def fit(self, listX, y=None):
        """A reference implementation of a fitting function for a transformer.

        Parameters
        ----------
        listX : list of array-like or sparse matrix of shape = [n_samples, n_features]
            The training input samples.
        y : None
            There is no need of a target in a transformer, yet the pipeline API
            requires this parameter.

        Returns
        -------
        self : object
            Returns self.
        """
        # check_array(X)

        #self.inputShape = X.shape

        return self

    def transform(self, X):
        """ A reference implementation of a transform function.

        Parameters
        ----------
        X : array-like of shape = [n_samples, n_features]
            The input samples.

        Returns
        -------
        X_transformed : array of int of shape = [n_samples, n_features]
            The array containing the element-wise square roots of the values
            in `X`
        """
        # Check is fit had been called
        #check_is_fitted(self, ['inputShape'])

        # Input validation
        #check_array(X)

        # Check that the input is of the same shape as the one passed
        # during fit.
        # if X.shape != self.inputShape:
        #     raise ValueError('Shape of input is different from what was seen'
        #                      'in `fit`')


        # startTime = max([pd.Timestamp(X.index.min()) for X in XList])
        X.index = pd.to_datetime(X.index)
        endTime = X.index.max()
        self.resamplingBase = endTime.second+0.1

        X = X[self.currencyPairList]
        X.drop_duplicates(inplace=True)

        # X_transformed_index = pd.date_range(start=startTime, end=endTime,
        #                                     freq=self.samplingFrequency)
        X_transformed_columns = pd.MultiIndex(levels=[[]]*2, labels=[[]]*2, names=['',''])
        X_transformed = pd.DataFrame(columns=X_transformed_columns)

        for quantity in self.quantityList:
            X_quantity = self.calculate_quantity(X, quantity)
            X_transformed = pd.concat([X_transformed, X_quantity], axis=1)
            del X_quantity

        # print("Sampled start: ", X_transformed.index[0], X.index[0])
        # print("Sampled end: ", X_transformed.index[-1], X.index[-1])
        # print("X index: ", X.index)

        # print("Sampled: ", X_transformed.shape[0]-80)
        return X_transformed

    def calculate_quantity(self, X, quantity):
        quantityRecipe = self.quantityRecipeDict[quantity]

        columnMask = X.columns.get_level_values(1) == quantityRecipe[0]
        if quantityRecipe[2] != 'all':
            lineTypeMask = X[self.currencyPairList]['type'] == quantityRecipe[2]
            X = X.loc[lineTypeMask, :]

        X_feature = quantityRecipe[1](X.loc[:, columnMask].resample(self.samplingFrequency,
                                                                    closed='left',
                                                                    base=self.resamplingBase,
                                                                    loffset=self.samplingFrequency))

        X_featureInfMask = X_feature.values == np.inf
        X_feature.fillna(value=quantityRecipe[3], method=quantityRecipe[4],
                         axis=0, inplace=True)
        X_feature[:][X_featureInfMask] += 1e-8
        labels = [X_feature.columns.labels[0], [0] * X_feature.columns.labels[0]]
        X_feature.columns.set_labels(labels, inplace=True)
        X_feature.columns.set_levels([quantity], level=1, inplace=True)

        return X_feature
