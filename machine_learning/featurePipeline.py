import numpy as np
import pandas as pd
import sklearn as sk

import nPeriodsReturner as nPR
import firstLinesDropper as fLD
import pandasImputer as pI

from sklearn.pipeline import Pipeline
from pandasFeatureUnion import PandasFeatureUnion as FeatureUnion

from sklearn.utils.validation import check_X_y, check_array, check_is_fitted
from sklearn.base import BaseEstimator, TransformerMixin


class FeaturePipeline(Pipeline):
    """ A transformer that returns the N periods returns of an input quantity

    Parameters
    ----------
    quantity : str
        Quantity to calculate the N periods return of
    nPeriodsList : list of int
        List of number of periods

    Attributes
    ----------
    inputShape : tuple
        The shape the data passed to :meth:`fit`
    """

    def __init__(self, nPeriodReturnCloseList, nPeriodReturnVolumeList,
                 nWindowsCloseList, nWindowsVolumeList,
                 nPeriodReturnMovAvCloseList, nPeriodReturnMovAvVolumeList):

        unionSteps = []
        unionSteps.append(('nPeriodReturnClose',
                           nPR.NPeriodsReturner(quantityList=['close'],
                                                nPeriodsList=nPeriodReturnCloseList)))
        unionSteps.append(('nPeriodReturnVolume',
                           nPR.NPeriodsReturner(quantityList=['volume'],
                                                nPeriodsList=nPeriodReturnCloseList)))

        unionSteps.append(('nPeriodReturnMovAvClose',
                           nPR.NPeriodsReturner(quantityList=['close_mov_av_'+str(n) for n in nWindowsCloseList],
                                                          nPeriodsList=nPeriodReturnMovAvCloseList)))
        unionSteps.append(('nPeriodReturnMovAvVolume',
                           nPR.NPeriodsReturner(quantityList=['volume_mov_av_'+str(n) for n in nWindowsVolumeList],
                                                nPeriodsList=nPeriodReturnMovAvVolumeList)))

        steps = []

        steps.append(('featureUnion', FeatureUnion(unionSteps)))

        self.nPeriodMax = max(nPeriodReturnCloseList+nPeriodReturnVolumeList
                              +nPeriodReturnMovAvCloseList+nPeriodReturnMovAvVolumeList)
        steps.append(('featureDropFirstLines',
                      fLD.FirstLinesDropper(self.nPeriodMax)))

        steps.append(('featureImputerNaN', pI.PandasImputer(missing_values='NaN',
                                                            strategy='median', axis=1)))
        # steps.append(('featureImputerInf', pI.PandasImputer(missing_values='inf',
        #                                                     strategy='median', axis=1)))

        super(FeaturePipeline, self).__init__(steps)
