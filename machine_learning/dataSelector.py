import numpy as np
import pandas as pd
import sklearn as sk
from sklearn.utils.validation import check_X_y, check_array, check_is_fitted
from sklearn.base import BaseEstimator, ClassifierMixin, TransformerMixin

class DataSelectionTransformer(BaseEstimator, TransformerMixin):
    """ An example transformer that returns the N periods returns

    Parameters
    ----------
    nPeriodsList : list of int
        List of number of periods

    Attributes
    ----------
    inputShape : tuple
        The shape the data passed to :meth:`fit`
    """

    def __init__(self, currencyPairs=['BTC_ETH'], period='5min'):
        self.currencyPairs=currencyPairs
        self.period = pd.Timedelta(period)

    def get_params(self, deep=True):
        return {'currencyPairs': self.currencyPairs,
                'period': self.period}

    def set_params(self, **parameters):
        for parameter, value in parameters.items():
            self.setattr(parameter, value)
        return self

    def fit(self, X, y=None):
        """A reference implementation of a fitting function for a transformer.

        Parameters
        ----------
        X : array-like or sparse matrix of shape = [n_samples, n_features]
            The training input samples.
        y : None
            There is no need of a target in a transformer, yet the pipeline API
            requires this parameter.

        Returns
        -------
        self : object
            Returns self.
        """
        check_array(X)

        self.inputShape = X.shape

        # Return the transformer
        return self

    def transform(self, X):
        """ A reference implementation of a transform function.

        Parameters
        ----------
        X : array-like of shape = [n_samples, n_features]
            The input samples.

        Returns
        -------
        X_transformed : array of int of shape = [n_samples, n_features]
            The array containing the element-wise square roots of the values
            in `X`
        """
        # Check is fit had been called
        check_is_fitted(self, ['inputShape'])

        # Input validation
        check_array(X)

        # Check that the input is of the same shape as the one passed
        # during fit.
        if X.shape != self.inputShape:
            raise ValueError('Shape of input is different from what was seen'
                             'in `fit`')
        X = X.loc[:, self.currencyPairs]
        X.index = pd.to_datetime(X.index)

        volumeColumnMask = X.columns.get_level_values(1) == 'volume'
        X.loc[:,volumeColumnMask] = X.loc[:, volumeColumnMask].rolling(self.period).sum()

        return X[X.index[0]+self.period:]
