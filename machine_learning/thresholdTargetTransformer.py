import numpy as np
import pandas as pd
import sklearn as sk
import dataSamplingTransformer as dST
import nPeriodsReturnTransformer as nPRT

from sklearn.preprocessing import Binarizer
from sklearn.pipeline import Pipeline
from sklearn.pipeline import FeatureUnion

from sklearn.utils.validation import check_X_y, check_array, check_is_fitted
from sklearn.base import BaseEstimator, ClassifierMixin, TransformerMixin

class TargetTransformer(BaseEstimator, TransformerMixin):
    """ A transformer that returns the target associated with our classfier pipeline

    Parameters
    ----------
    currencyPairList : list of str
        The list of all currencyPairs we want to make a prediction for
    samplingPeriod : str
        The sampling period for the input resampling
    tradingPeriodList : list of str
        The list of all the trading period we want to have prediction for
    threshold : float
        The threshold above which the prediction is one

    Attributes
    ----------
    inputShape : tuple
        The shape the data passed to :meth:`fit`
    union : FeatureUnion
        The union of the pipelines corresponding to each tradingPeriod in tradingPeriodList
    """

    def __init__(self, currencyPairList=['BTC_ETH'], samplingFrequency='30s',
                 tradingPeriodList=['5min'], threshold=0.007):
        self.currencyPairList = currencyPairList
        self.samplingFrequency = samplingFrequency
        self.tradingPeriodList = tradingPeriodList
        self.threshold = threshold

        pipelines = [('target_period_'+tradingPeriod, self.make_pipeline(tradingPeriod)) for tradingPeriod in tradingPeriodList]
        self.union = FeatureUnion(pipelines)

    def make_pipeline(self, tradingPeriod):
        nPeriod = int(pd.Timedelta(tradingPeriod)/pd.Timedelta(self.samplingFrequency))
        dataSamplingTransformer = dST.DataSamplingTransformer(currencyPairList=self.currencyPairList,
                                                              samplingFrequency=self.samplingFrequency,
                                                              quantityList=['close'])
        nPeriodReturnTransformer = nPRT.NPeriodsReturnTransformer(quantity='close',
                                                                  nPeriodsList=[nPeriod])
        binarizer = Binarizer(threshold=self.threshold)
        return Pipeline([('dataSampling', dataSamplingTransformer),
                         ('1periodReturn', nPeriodReturnTransformer),
                         ('binarizer', binarizer)])


    def get_params(self, deep=True):
        return {'currencyPairList': self.currencyPairList,
                'samplingFrequency': self.samplingFrequency,
                'tradingPeriodList': self.tradingPeriodList,
                'threshold': self.threshold}

    def set_params(self, **parameters):
        for parameter, value in parameters.items():
            self.setattr(parameter, value)
        return self

    def fit(self, XList=None, yList):
        """A reference implementation of a fitting function for a transformer.

        Parameters
        ----------
        XList : a list of pandas.DataFrame
            The list of training input samples for each currencyPair. There is no need of
            features in this custom target transformer.
        yList : a list of pandas.DataFrame
            The list of training input samples for each currencyPair.

        Returns
        -------
        self : object
            Returns self.
        """

        # Return the transformer
        return self

    def transform(self, XList=None, yList):
        """ Implementation of the transform function that returns the target

        Parameters
        ----------
        XList : a list of pandas.DataFrame
            The list of training input samples for each currencyPair. There is no need of
            features in this custom target transformer.
        yList : a list of pandas.DataFrame
            The list of training input samples for each currencyPair.

        Returns
        -------
        y_transformed : pd.DataFrame
            The DataFrame that contains the binary target for each currencyPair in
            currencyPairList and each tradingPeriod in tradingPeriodList for a given
            sampingPeriodand threshold
        """

        yList = [y for y in yList if (y.columns.get_level_values(0)[0] in self.currencyPairList)]

        # Input validation
        #for y in yList:
        #    y[:] = check_array(y.values)

        startTime = max([y.index.min() for y in yList])
        endTime = min([y.index.max() for y in yList])
        y_transformed_index = pd.date_range(start=startTime, end=endTime,
                                            freq=self.samplingFrequency)
        y_transformed_columns = pd.MultiIndex(levels=[self.currencyPairList,
                                                      ["target_"+str(self.threshold)]],
                                              labels=[[0],[0]], names=['',''])
        y_transformed = pd.DataFrame(index=y_transformed_index[1:],
                                     columns=y_transformed_columns)

        y_transformed[:] = self.union.transform(yList)

        return y_transformed
