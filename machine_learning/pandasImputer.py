import numpy as np

from sklearn.preprocessing import Imputer

from sklearn.utils.validation import check_X_y, check_array, check_is_fitted
from sklearn.utils.multiclass import unique_labels

class PandasImputer(Imputer):
    """Pandas-compatible Imputation transformer for completing missing values.

    Parameters
    ----------
    missing_values : integer or "NaN", optional (default="NaN")
        The placeholder for the missing values. All occurrences of
        `missing_values` will be imputed. For missing values encoded as np.nan,
        use the string value "NaN".

    strategy : string, optional (default="mean")
        The imputation strategy.

        - If "mean", then replace missing values using the mean along
          the axis.
        - If "median", then replace missing values using the median along
          the axis.
        - If "most_frequent", then replace missing using the most frequent
          value along the axis.

    axis : integer, optional (default=0)
        The axis along which to impute.

        - If `axis=0`, then impute along columns.
        - If `axis=1`, then impute along rows.

    verbose : integer, optional (default=0)
        Controls the verbosity of the imputer.

    copy : boolean, optional (default=True)
        If True, a copy of X will be created. If False, imputation will
        be done in-place whenever possible. Note that, in the following cases,
        a new copy will always be made, even if `copy=False`:

        - If X is not an array of floating values;
        - If X is sparse and `missing_values=0`;
        - If `axis=0` and X is encoded as a CSR matrix;
        - If `axis=1` and X is encoded as a CSC matrix.

    Attributes
    ----------
    statistics_ : array of shape (n_features,)
        The imputation fill value for each feature if axis == 0.

    Notes
    -----
    - When ``axis=0``, columns which only contained missing values at `fit`
      are discarded upon `transform`.
    - When ``axis=1``, an exception is raised if there are rows for which it is
      not possible to fill in the missing values (e.g., because they only
      contain missing values).
    """
    def __init__(self, missing_values="NaN", strategy="mean",
                 axis=0, verbose=0, copy=True):
        super(PandasImputer, self).__init__(missing_values, strategy, axis, verbose, copy)


    def transform(self, X):
        """Impute all missing values in X.

        Parameters
        ----------
        X : {array-like, sparse matrix}, shape = [n_samples, n_features]
            The input data to complete.
        """
        X[:] = super(PandasImputer, self).transform(X[:])
        return X


    # def fit(self, X, y=None):
    #     """Fit the imputer on X.

    #     Parameters
    #     ----------
    #     X : {array-like, sparse matrix}, shape (n_samples, n_features)
    #         Input data, where ``n_samples`` is the number of samples and
    #         ``n_features`` is the number of features.

    #     Returns
    #     -------
    #     self : Imputer
    #         Returns self.
    #     """
    #     # Check parameters
    #     allowed_strategies = ["mean", "median", "most_frequent"]
    #     if self.strategy not in allowed_strategies:
    #         raise ValueError("Can only use these strategies: {0} "
    #                          " got strategy={1}".format(allowed_strategies,
    #                                                     self.strategy))

    #     if self.axis not in [0, 1]:
    #         raise ValueError("Can only impute missing values on axis 0 and 1, "
    #                          " got axis={0}".format(self.axis))

    #     # Since two different arrays can be provided in fit(X) and
    #     # transform(X), the imputation data will be computed in transform()
    #     # when the imputation is done per sample (i.e., when axis=1).
    #     if self.axis == 0:
    #         X = check_array(X, accept_sparse='csc', dtype=np.float64,
    #                         force_all_finite=False)

    #         if sparse.issparse(X):
    #             self.statistics_ = self._sparse_fit(X,
    #                                                 self.strategy,
    #                                                 self.missing_values,
    #                                                 self.axis)
    #         else:
    #             self.statistics_ = self._dense_fit(X,
    #                                                self.strategy,
    #                                                self.missing_values,
    #                                                self.axis)

    #     return self

    # def transform(self, X):
    #     """Impute all missing values in X.

    #     Parameters
    #     ----------
    #     X : {array-like, sparse matrix}, shape = [n_samples, n_features]
    #         The input data to complete.
    #     """
    #     if self.axis == 0:
    #         check_is_fitted(self, 'statistics_')
    #         X = check_array(X, accept_sparse='csc', dtype=FLOAT_DTYPES,
    #                         force_all_finite=False, copy=self.copy)
    #         statistics = self.statistics_
    #         if X.shape[1] != statistics.shape[0]:
    #             raise ValueError("X has %d features per sample, expected %d"
    #                              % (X.shape[1], self.statistics_.shape[0]))

    #     # Since two different arrays can be provided in fit(X) and
    #     # transform(X), the imputation data need to be recomputed
    #     # when the imputation is done per sample
    #     else:
    #         X = check_array(X, accept_sparse='csr', dtype=FLOAT_DTYPES,
    #                         force_all_finite=False, copy=self.copy)

    #         if sparse.issparse(X):
    #             statistics = self._sparse_fit(X,
    #                                           self.strategy,
    #                                           self.missing_values,
    #                                           self.axis)

    #         else:
    #             statistics = self._dense_fit(X,
    #                                          self.strategy,
    #                                          self.missing_values,
    #                                          self.axis)

    #     # Delete the invalid rows/columns
    #     invalid_mask = np.isnan(statistics)
    #     valid_mask = np.logical_not(invalid_mask)
    #     valid_statistics = statistics[valid_mask]
    #     valid_statistics_indexes = np.where(valid_mask)[0]
    #     missing = np.arange(X.shape[not self.axis])[invalid_mask]

    #     if self.axis == 0 and invalid_mask.any():
    #         if self.verbose:
    #             warnings.warn("Deleting features without "
    #                           "observed values: %s" % missing)
    #         X = X[:, valid_statistics_indexes]
    #     elif self.axis == 1 and invalid_mask.any():
    #         raise ValueError("Some rows only contain "
    #                          "missing values: %s" % missing)

    #     # Do actual imputation
    #     if sparse.issparse(X) and self.missing_values != 0:
    #         mask = _get_mask(X.data, self.missing_values)
    #         indexes = np.repeat(np.arange(len(X.indptr) - 1, dtype=np.int),
    #                             np.diff(X.indptr))[mask]

    #         X.data[mask] = valid_statistics[indexes].astype(X.dtype,
    #                                                         copy=False)
    #     else:
    #         if sparse.issparse(X):
    #             X = X.toarray()

    #         mask = _get_mask(X, self.missing_values)
    #         n_missing = np.sum(mask, axis=self.axis)
    #         values = np.repeat(valid_statistics, n_missing)

    #         if self.axis == 0:
    #             coordinates = np.where(mask.transpose())[::-1]
    #         else:
    #             coordinates = mask

    #         X[coordinates] = values

    #     return X
