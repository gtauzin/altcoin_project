import numpy as np
import pandas as pd
import sklearn as sk

import identityTransformer as idT
import firstLinesDropper as fLD
import nMovingAverager as nMA
import pandasImputer as pI

from sklearn.pipeline import Pipeline
from pandasFeatureUnion import PandasFeatureUnion as FeatureUnion


class PreprocessingPipeline(Pipeline):
    """ A transformer that returns the N periods returns of an input quantity

    Parameters
    ----------
    quantity : str
        Quantity to calculate the N periods return of
    nPeriodsList : list of int
        List of number of periods

    Attributes
    ----------
    inputShape : tuple
        The shape the data passed to :meth:`fit`
    """

    def __init__(self, currencyPairList, samplingFrequency, quantityList,
                 nWindowsCloseList, nWindowsVolumeList):
        steps = []
        unionSteps = []
        unionSteps.append(('identity', idT.IdentityTransformer()))

        if nWindowsCloseList:
            unionSteps.append(('nMovingAverageClose',
                               nMA.NMovingAverager(quantityList=['close'],
                                                   nWindowsList=nWindowsCloseList)))
        if nWindowsVolumeList:
            unionSteps.append(('nMovingAverageVolume',
                               nMA.NMovingAverager(quantityList=['volume'],
                                                   nWindowsList=nWindowsVolumeList)))

        steps.append(('preprocessingUnion',
                      FeatureUnion(unionSteps)))

        self.nPeriodMax = max([1]+nWindowsCloseList+nWindowsVolumeList) - 1
        steps.append(('preprocessingDropFirstLines',
                      fLD.FirstLinesDropper(self.nPeriodMax)))

        steps.append(('preprocessingImputer',
                      pI.PandasImputer(missing_values='NaN', strategy='median', axis=1)))

        super(PreprocessingPipeline, self).__init__(steps)
