import pickle
import numpy as np
import sys
sys.path.insert(1, './machine_learning')

import dataSampler as dS
import preprocessingPipeline as pP
import lastLinesDropper as lLD
import firstLinesDropper as fLD
import featurePipeline as fP
import targetPipeline as tP

from sklearn.pipeline import Pipeline

import sklearn.preprocessing as prep
import sklearn.feature_selection as slct
import sklearn.ensemble as ens


class Classifier(Pipeline):
    """ Our generic Pipeline that gather all steps required to train

    Parameters
    ----------
    currencyPairList : list of str
        The list of all currencyPairs we want to make a prediction for
    samplingPeriod : str
        The sampling period for the input resampling
    tradingPeriodList : list of str
        The list of all the trading period we want to have prediction for
    threshold : float
        The threshold above which the prediction is one

    Attributes
    ----------
    inputShape : tuple
        The shape the data passed to :meth:`fit`
    union : FeatureUnion
        The union of the pipelines corresponding to each tradingPeriod in tradingPeriodList
    """

    def __init__(self, targetCurrencyPairList=['BTC_ETH'],
                 featureCurrencyPairList=['BTC_ETH', 'USDT_BTC', 'USDT_ETH'],
                 samplingFrequency='30s', quantityList=['close', 'volume'],
                 tradingPeriodList=['5min'], thresholdList=[0.007],
                 nWindowsCloseList=[4], nWindowsVolumeList=[4],
                 nPeriodReturnCloseList=[1,2,3,4,5],
                 nPeriodReturnVolumeList=[1,2,3,4,5],
                 nPeriodReturnMovAvCloseList=[100,200,300,400,500],
                 nPeriodReturnMovAvVolumeList=[100,200,300,400,500]):
        transformerSteps = []
        transformerSteps.append(('dataSampling',
                                 dS.DataSampler(currencyPairList=featureCurrencyPairList,
                                                samplingFrequency=samplingFrequency,
                                                quantityList=quantityList)))
        self.preprocessingPipeline = pP.PreprocessingPipeline(featureCurrencyPairList,
                                                              samplingFrequency,
                                                              quantityList,
                                                              nWindowsCloseList,
                                                              nWindowsVolumeList)
        transformerSteps.append(('preprocessing', self.preprocessingPipeline))

        self.featurePipeline = fP.FeaturePipeline(nPeriodReturnCloseList,
                                                  nPeriodReturnVolumeList,
                                                  nWindowsCloseList,
                                                  nWindowsVolumeList,
                                                  nPeriodReturnMovAvCloseList,
                                                  nPeriodReturnMovAvVolumeList)
        transformerSteps.append(('feature', self.featurePipeline))
        self.transformerPipeline = Pipeline(transformerSteps)

        estimatorSteps = []
        estimatorSteps.append(('scaler', prep.StandardScaler(copy=False)))

        estimator = ens.RandomForestClassifier()

        #estimatorSteps.append(('featureSelection', slct.SelectFromModel(estimator)))
        estimatorSteps.append(('estimator', estimator))

        self.estimatorPipeline = Pipeline(estimatorSteps)

        self.targetPipeline = tP.TargetPipeline(currencyPairList=targetCurrencyPairList,
                                                samplingFrequency=samplingFrequency,
                                                tradingPeriodList=tradingPeriodList,
                                                thresholdList=thresholdList)
        super(Classifier, self).__init__(transformerSteps+estimatorSteps)


        nPeriodRequired = self.preprocessingPipeline.nPeriodMax \
                          + self.featurePipeline.nPeriodMax

        self.marketParameters = {'currencyPair': targetCurrencyPairList[0],
                                 'tradingPeriod': tradingPeriodList[0],
                                 'featureCurrencyPairList': featureCurrencyPairList,
                                 'samplingFrequency': samplingFrequency,
                                 'nPeriodRequired': nPeriodRequired}

    def fit(self, XList):
        """A reference implementation of a fitting function for a transformer.

        Parameters
        ----------
        listX : list of array-like or sparse matrix of shape = [n_samples, n_features]
            The training input samples.
        y : None
            There is no need of a target in a transformer, yet the pipeline API
            requires this parameter.

        Returns
        -------
        self : object
            Returns self.
        """
        # check_array(X)

        #self.inputShape = X.shape

        print('Transforming target...')
        y = self.targetPipeline.fit(XList).transform(XList)
        print('...done!')

        print('Transforming feature...')
        X = self.transformerPipeline.fit(XList).transform(XList)
        print('...done!')

        print('Enforcing common index...')
        commonIndex = X.index.intersection(y.index)
        X = X.loc[commonIndex]
        y = y.loc[commonIndex]

        print('  Shape of X: ', X.shape)
        print('  Shape of y: ', y.shape)

        print('Fitting...')
        self.estimatorPipeline.fit(X, y)
        print('...done!')

        return self

    def transform(self, XList):
        """ A reference implementation of a transform function.

        Parameters
        ----------
        X : array-like of shape = [n_samples, n_features]
            The input samples.

        Returns
        -------
        X_transformed : array of int of shape = [n_samples, n_features]
            The array containing the element-wise square roots of the values
            in `X`
        """
        # Check is fit had been called
        #check_is_fitted(self, ['inputShape'])

        # Input validation
        #check_array(X)

        # Check that the input is of the same shape as the one passed
        # during fit.
        # if X.shape != self.inputShape:
        #     raise ValueError('Shape of input is different from what was seen'
        #                      'in `fit`')

        return self.transformerPipeline.transform(XList)


    def save(self, filename):
        pickle.dump(self, open(filename, 'wb'))

    def load(self, filename):
        return pickle.load(open(filename, 'rb'))

    def split(self, dataFrame):

        # We split the dataFrame based on datesTraining and datesTesting
        self.X_train = features.ix[self.datesTraining]
        self.X_test = features.ix[self.datesTesting]
        self.y_train = dataFrame.ix[self.datesTraining][key1DReturnCurrencyPairToForecast]
        self.y_test = dataFrame.ix[self.datesTesting][key1DReturnCurrencyPairToForecast]

    def predict(self, X):
        X = self.transformerPipeline.transform(X)
        nPeriodRequired = self.preprocessingPipeline.nPeriodMax \
                          + self.featurePipeline.nPeriodMax
        X = X.ix[-1].reshape(1, -1)
        return self.estimatorPipeline.predict(X)
