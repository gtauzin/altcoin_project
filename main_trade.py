import os
import pandas as pd
import numpy as np
import sys

sys.path.insert(1, '.')
import classifier as clf
import market as mkt
import strategy as strat
import time

inputDirectory = './input/poloniex/'


if __name__ == "__main__":
    filename = 'fittedClassifier.pkl'
    # Select classifier here
    classifier = clf.Classifier().load(filename)
    parameters = classifier.marketParameters

    print("Running with classifier parameters: ", parameters)

    startDate = pd.Timestamp('2017-11-23-02:00:00')
    endDate = pd.Timestamp('2017-12-23-02:00:00')

    X_columns = pd.MultiIndex(levels=[['date'], ['']], labels=[[]]*2, names=['date',''])
    X = pd.DataFrame(columns=X_columns)

    for currencyPair in parameters['featureCurrencyPairList']:
        print('Reading data file for', currencyPair)
        filename = currencyPair + '.csv'
        X_currencyPair = pd.read_csv(inputDirectory+filename, header=[0, 1], index_col=[0])
        X_currencyPair.index = pd.to_datetime(X_currencyPair.index)
        X_currencyPair = X_currencyPair[startDate:endDate]
        print('Done reading!')

        X_currencyPair['date', ''] = X_currencyPair.index
        X_currencyPair.index = pd.Index(X_currencyPair[currencyPair, 'globalTradeID'],
                                        name = 'globalTradeID')
        X_currencyPair.drop('globalTradeID', axis=1, level=1, inplace=True)
        X = pd.merge(X, X_currencyPair, on='date', left_index=True, right_index=True, how='outer')

    X.set_index('date', inplace=True)

	# Select market
    market = mkt.VirtualPoloniex(**classifier.marketParameters, X=X)

    # Select strategy
    strategy = strat.BasicStrategy(market, classifier)

    # Get dataframe close only for the currency to forecast

    # Simulates trading over the datesValidating
    while(market.is_final_iteration() == False):
        strategy.apply()

    print(strategy.observableDict)
