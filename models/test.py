import time
import numpy as np
import pandas as pd
import os
from utils import altcoinUtils


def execute():
	'''
	The target variable is BTC_ETH, in particular the discrete variable:
	daily return positive or negative (or above, below threshold).
	The features are the n days returns of the other currencyPairs, plus the 24hr volumes.

	We use Decision trees, random forest and other.
	:return:
	'''

	# Read data
	frequency = "5min"
	startDate = '2017-01-01-23:00:00'
	endDate = '2017-06-22-22:00:00'
	# make sure that you load data from csvs with the correct frequency
	# the data rang from the csv file is from 2017-03-17 to 2017-05-22 21:30:00
	dates = pd.date_range(startDate, endDate, freq=frequency)
	# make sure than the datapoint corresponding to the initial date is different from zero!

	# Define percentageTreshold: Ex. 3 * 0.01 means 3%
	percentageTreshold = 0.7 * 0.01
	# Some accuracies (on train):
	# 30 min 0.86
	# 15min 0.83
	# 5min 0.82
	# Try to improve these, without overfitting. The test values should be 10-15% lower than these,
	#  if more I'm afraid you are overfitting
	symbols = get_available_symbols()
	currency_pair_to_forecast = "BTC_ETH"

	# Time the run
	start_time = time.time()

	GENERATE_DATA_FRAME = False  # set to true if you want to build a the dataset
	if GENERATE_DATA_FRAME:
		# setup the dataframe and print it to csv file. It's needed to run this line only once
		# (if currencyPairToForecast,symbols, dates or frequency are not changed).
		# It's pretty slow...
		final_df = setup_dataframe(currency_pair_to_forecast, symbols, dates, frequency, percentageTreshold)
		print("Full dataframe built \n Duration: %s" % (time.time() - start_time))

	# merge all the column labels
	features_labels = generate_feature_labels(symbols, currency_pair_to_forecast)

	#############
	# load the dataframe from csv
	final_df = altcoinUtils.load_data(frequency, input_data_dir_path)
	X_train, X_test, y_train, y_test = altcoinUtils.split_train_test_data(final_df, 0.25, currency_pair_to_forecast)

	print(final_df)
	print("Built the dataframe, data loaded in ", time.time() - start_time, " sec")

	##########################
	# PICKING THE ALGO
	# which algo do you want to pick?
	##########################

	classifier_options = ["DecisionTree", "SGD", "SVM", "NB", "LinearSVC", "Knear", "RandomForests", "LogReg",
						  "ERandomizedTrees", "GradientBoosting", "AdaBoost",
						  "ExtremeGradientBoosting", "Voting"]

	classifier_chosen = altcoinUtils.display_menu("classifier", classifier_options)
	classifier_chosen = classifier_options[classifier_chosen]
	#classifier_chosen = classifier_options[6]

	start_time_classifier = time.time()

	# train and test
	use_the(classifier_chosen, X_train, y_train, X_test, y_test, features_labels, currency_pair_to_forecast,
			final_df, percentageTreshold, frequency, startDate, endDate, symbols)

	end = time.time()
	print("classifier analysis completed in ", end - start_time_classifier, " sec")
