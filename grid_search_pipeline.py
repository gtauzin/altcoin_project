import time
import utils


def perform_grid_search(classifier, X_train, y_train):

	############################################
	# Pipeline and GridSearchCV start
	############################################
	from sklearn.feature_selection import mutual_info_classif
	from sklearn.feature_selection import SelectKBest
	from sklearn.model_selection import GridSearchCV
	from sklearn.pipeline import Pipeline

	feature_selector = SelectKBest(score_func= mutual_info_classif, k = 5)

	# feature selection and classifier
	pipe = Pipeline([
		('featureSelection', feature_selector),
		('classify', classifier)
	])

	############################################
	# set up param_grid for two different feature selection algorithm
	# 1. PCA
	# 2. SelectKBest
	############################################

	numberFeatures = X_train.shape[1]
	N_FEATURES_OPTIONS = [5] #[15,25,35]
	max_depths =  [7] #[5, 7, None]
	n_estimators = [10, 100, 1000]
	criterions = ['gini', 'entropy']
	min_samples_leaf =  [5] #[1, 3, 5]
	min_samples_split =  [6] #[2, 4, 6]


	# TODO We need to select programmatically the best treshold

	# parameters to grid search. Notice the (odd) notation with __.
	param_grid = [
		{
			'featureSelection__k': N_FEATURES_OPTIONS,
			#'classify__learning_rate': learning_rates,
			'classify__max_depth': max_depths,
			# 'classify__n_estimators': n_estimators,
			# 'classify__criterion': criterions,
			'classify__min_samples_leaf': min_samples_leaf,
			'classify__min_samples_split': min_samples_split,
		},
	]

	print("grid searching ...")
	start_time = time.time()

	number_of_cross_validations = 3
	grid = GridSearchCV(pipe, cv=number_of_cross_validations, n_jobs=1, param_grid=param_grid)
	# fit the classifier with the best parameters found in gridsearch. It will be used later to make predictions.
	classifier = grid.fit(X_train, y_train)

	print(X_train.shape)

	# print grid search results
	print("Best Estimator: ", grid.best_estimator_)
	print("Best Parameters: ", grid.best_params_)
	print("Best Score: ", grid.best_score_)

	# evaluation of the results
	utils.measure_performance(X_train, y_train, classifier, show_accuracy=True, show_classification_report=True,
							  show_confusion_matrix=True)

	# print the name of the selected columns
	finalFeatureIndices = grid.best_estimator_.named_steps["featureSelection"].get_support(indices=True)
	finalFeatureList = [X_train.columns.values.tolist()[i] for i in finalFeatureIndices]
	print("Selected Features: ", finalFeatureList)

	utils.featureImportance(grid.best_estimator_.named_steps['classify'], finalFeatureList)

	end = time.time()
	print("completed in ", end - start_time, " sec")
	############################################
	# Pipeline and GridSearchCV end
	############################################

	return classifier