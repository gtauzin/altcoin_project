### Prerequisites

Install miniconda which will be the default package manager.
The latest version can be downloaded from [here](https://conda.io/miniconda.html).

### Installation

1. Create a conda environment

    conda env create -f environment.yml

2. Activate the conda environment

    source activate altcoin

To update the environment

    conda env export > environment.yml

## Contributing

We have a bunch of issues to be done, commented etc ! Have a look [here](https://gitlab.com/altcoinForecast/classificationAltcoin/boards)!
Feel free to create a new issue with some suggestions or idea !!

### Python Guidelines

Following [The Hitchiker's Guide to Python Code](http://docs.python-guide.org/en/latest/#writing-great-python-code)


### Docs

You find relevant documentation about the models in [docs](/docs).
Please make sure you document the models you're using and your references there.

For more detailed info, see the individual README in the corresponding folder.
