import sys

sys.path.insert(1, '.')
import classifier as clf
import market as mkt

class Strategy(object):

    #TODO: Use dictionnaries
    def __init__(self, market, classifier):
        self.market = market
        self.classifier = classifier
        self.sellAtNextPeriod = False
        self.amountBought = 0
        self.amountSold = 0

        self.observableDict = {}
        self.observableDict['initialHolding'] = market.primaryCurrencyHolding
        self.observableDict['numberTrades'] = 0
        self.observableDict['numberTradesLoss'] = 0
        self.observableDict['numberTradesGain'] = 0
        self.observableDict['sumRelativeGain'] = 0
        self.observableDict['sumRelativeLoss'] = 0
        self.observableDict['maxRelativeGain'] = 0
        self.observableDict['maxRelativeLoss'] = 0

    def update_observables(self):
        self.observableDict['numberTrades'] += 1
        self.observableDict['primaryCurrencyHolding'] = self.market.primaryCurrencyHolding
        self.observableDict['secondaryCurrencyHolding'] = self.market.secondaryCurrencyHolding

        if self.amountSold < self.amountBought:
            self.observableDict['numberTradesLoss'] += 1

            relativeLoss = (self.amountBought - self.amountSold)/self.amountBought
            self.observableDict['sumRelativeLoss'] += relativeLoss
            self.observableDict['maxRelativeLoss'] = max(self.observableDict['maxRelativeLoss'],
                                                         relativeLoss)
            self.observableDict['averageRelativeLoss'] = \
                self.observableDict['sumRelativeLoss']/self.observableDict['numberTradesLoss']

        else:
            self.observableDict['numberTradesGain'] += 1

            relativeGain = (self.amountSold - self.amountBought)/self.amountBought
            self.observableDict['sumRelativeGain'] += relativeGain
            self.observableDict['maxRelativeGain'] = max(self.observableDict['maxRelativeGain'], relativeGain)
            self.observableDict['averageRelativeGain'] = \
                self.observableDict['sumRelativeGain']/self.observableDict['numberTradesGain']

        self.observableDict['returnOnInvestment'] = \
            self.amountSold/self.observableDict['initialHolding']

        self.observableDict['percentageProfitableTrades'] = \
            self.observableDict['numberTradesGain']/self.observableDict['numberTrades'] * 100


class BasicStrategy(Strategy):
    def __init__(self, market, classifier):
        super(BasicStrategy, self).__init__(market, classifier)

    def apply(self):
        # TODO we should log on some txt file and send email after a trade

        # If we bought at the last period, we sell at the current one
        if self.sellAtNextPeriod == True:
            self.sellAtNextPeriod = False

            # Sell at market price, minus some small quantity to be sure that the order is executed
            self.amountSold = self.market.sell()

            # Observables update for virtual trading
            self.update_observables()

        # get current features
        currentX = self.market.query_current_raw_data()

        # compute the relevant features and predict
        prediction = self.classifier.predict(currentX)
        print('Predicting: ', self.market.endTime, prediction,
              self.market.primaryCurrencyHolding)

        # Buys if the classifier predicts so
        if int(prediction[-1]) == 1:
            self.sellAtNextPeriod = True

            # Buy at market price, minus some small quantity to be sure for the order to pass
            self.amountBought = self.market.buy()
