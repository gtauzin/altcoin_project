import os
import pandas as pd
import sys
sys.path.append('../')
import utils


path = utils.get_path("input/raw/30sec/2017-05-01BTC_ETH_USDT2017-11-06.csv")
print(path)


#print(input_dataframe_path)
dataframe = pd.read_csv(path,index_col = 0 ,parse_dates=True,usecols=[0,1,2,3,4,5,6], na_values=["nan"])
print(dataframe)
